package id.solfa.solfagaming.tools;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Ratri on 10/10/2017.
 */

public class FormatTools {

    public  String getRupiahFormat (String i){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###,###");
        String formattedString = formatter.format(Double.parseDouble(i));
        formattedString = formattedString.replaceAll(",",".");
        return "IDR "+formattedString;
    }
}
