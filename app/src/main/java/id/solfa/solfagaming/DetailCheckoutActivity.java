package id.solfa.solfagaming;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import id.solfa.solfagaming.fragment.CheckoutDetailFragment;
import id.solfa.solfagaming.fragment.CheckoutProductFragment;
import id.solfa.solfagaming.model.response.CheckoutResponse;

/**
 * Created by Ratri on 10/21/2017.
 */

public class DetailCheckoutActivity extends BaseActivity {
    ViewPager viewPager;
    TabLayout tabLayout;
    CheckoutResponse.Checkout checkout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_checkout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Intent i = getIntent();
        String query="";
        if(i.getExtras()!=null){
            checkout=(CheckoutResponse.Checkout)i.getSerializableExtra("model");
            toolbar.setTitle("#"+checkout.getInvoice_no());
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        //viewpager
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment( CheckoutDetailFragment.Instance(checkout), "Detail");
        adapter.addFragment(CheckoutProductFragment.Instance(checkout), "Products");
        adapter.addFragment(new PaymentFragment(), "Pembayaran");
        viewPager.setAdapter(adapter);
    }

    private   class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
