package id.solfa.solfagaming.retrofit;

import id.solfa.solfagaming.model.form.CheckoutForm;
import id.solfa.solfagaming.model.form.LoginFacebookForm;
import id.solfa.solfagaming.model.response.AddressResponse;
import id.solfa.solfagaming.model.response.AdressResponse;
import id.solfa.solfagaming.model.response.CartResponse;
import id.solfa.solfagaming.model.response.CategoryResponse;
import id.solfa.solfagaming.model.response.AddCheckoutResponse;
import id.solfa.solfagaming.model.response.CheckoutResponse;
import id.solfa.solfagaming.model.response.CityResponse;
import id.solfa.solfagaming.model.response.DistrictResponse;
import id.solfa.solfagaming.model.response.LoginResponse;
import id.solfa.solfagaming.model.response.ProductsResponse;
import id.solfa.solfagaming.model.response.ProfilResponse;
import id.solfa.solfagaming.model.response.ProvinsiResponse;
import id.solfa.solfagaming.model.response.ServerResponse;
import id.solfa.solfagaming.model.response.SubDistrictResponse;
import id.solfa.solfagaming.model.response.UpdateCartResponse;
import id.solfa.solfagaming.model.form.AddressForm;
import id.solfa.solfagaming.model.form.CartForm;
import id.solfa.solfagaming.model.form.LoginForm;
import id.solfa.solfagaming.model.form.ProductSearchForm;
import id.solfa.solfagaming.model.form.RegisterForm;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Ratri on 8/23/2017.
 */

public interface AllService {

        @Headers({"Content-Type:application/json"})
        @POST("v1/products/search")
        Call<ProductsResponse> searchProduct(@Body ProductSearchForm form);

        @GET("v1/users/addresses")
        Call<AdressResponse> getAdress(@Header("Authorization") String token);

        @GET("v1/locations/provinces")
        Call<ProvinsiResponse> getProvinces();

        @POST("v1/users/carts/add")
        Call<UpdateCartResponse> postAddCart(@Body CartForm cartForm, @Header("Authorization") String token);

        @GET("v1/users/carts")
        Call<CartResponse> getCart(@Header("Authorization") String token);

        @Headers({"Content-Type: application/json"})
        @POST("v1/oauth/token")
        Call<LoginResponse> login(@Body LoginForm loginForm);

        @Headers({"Content-Type:application/json"})
        @POST("v1/oauth/token")
        Call<LoginResponse> loginFacebook(@Body LoginFacebookForm Form);

        @GET("v1/users/profile")
        Call<ProfilResponse> getProfil(@Header("Authorization") String token);

        @Headers({"Content-Type:application/json"})
        @POST("v1/users/register")
        Call<ServerResponse> register(@Body RegisterForm registerForm);

        @Headers({"Content-Type:application/json"})
        @POST("v1/users/carts/update")
        Call<UpdateCartResponse> postUpdateCart(@Body CartForm cartForm,@Header("Authorization") String token);

        @GET("v1/products")
        Call<ProductsResponse> getProduk(@Query("page") int page, @Query("limit") int limit);

        @GET("v1/locations/cities")
        Call<CityResponse> getCity(@Query("province_id") String id);

        @GET("v1/locations/districts")
        Call<DistrictResponse> getDistrict(@Query("city_id") String id);

        @GET("v1/locations/subdistricts")
        Call<SubDistrictResponse> getSubDistrict(@Query("district_id") String id);

        @Headers({"Content-Type:application/json"})
        @POST("v1/users/addresses/add")
        Call<AddressResponse> addAddress(@Body AddressForm form,  @Header("Authorization") String token);

        @GET("/v1/categories")
        Call<CategoryResponse> getCategory();

        @Headers({"Content-Type:application/json"})
        @POST("/v1/users/checkouts/add")
        Call<AddCheckoutResponse> addCheckout(@Body CheckoutForm form, @Header("Authorization") String token);

        @Headers({"Content-Type:application/json"})
        @GET("/v1/users/checkouts")
        Call<CheckoutResponse> checkout(@Header("Authorization") String token);

}
