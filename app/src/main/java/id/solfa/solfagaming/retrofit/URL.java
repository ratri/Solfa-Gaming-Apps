package id.solfa.solfagaming.retrofit;

/**
 * Created by Ratri on 10/10/2017.
 */

public class URL {
    public static final String BASE_IMG ="https://s3-ap-southeast-1.amazonaws.com/assets.solfagaming.com/";
    public static final String BASE_IMG_BANNER ="https://s3-ap-southeast-1.amazonaws.com/assets.solfagaming.com/banners/";
    public static final String BASE_URL = "http://api.farizal.id/";
}
