package id.solfa.solfagaming;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import id.solfa.solfagaming.adapter.CheckoutCartAdapter;
import id.solfa.solfagaming.model.form.CheckoutForm;
import id.solfa.solfagaming.model.response.AdressResponse;
import id.solfa.solfagaming.model.response.AddCheckoutResponse;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/12/2017.
 */

public class AddCheckoutActivity extends BaseActivity {
    RecyclerView recyclerView;
    CheckoutCartAdapter adapter;
    public Button btnTotal, btnCheckout;
    TextView txtCartKosong;
    AdressResponse.Addresses addresses;
    public TextView txtNama, txtEmail, txtNoHp, txtAlamat, tvTotal;
    String cartId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_checkout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Checkout");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new CheckoutCartAdapter(this, getToken());
        recyclerView = (RecyclerView) findViewById(R.id.rv_cart);
        txtCartKosong = (TextView) findViewById(R.id.cart_kosong) ;
//        btnTotal = (Button) findViewById(R.id.btn_total) ;
        btnCheckout = (Button) findViewById(R.id.btn_checkout) ;
        txtNama = (TextView) findViewById(R.id.tv_nama);
        txtNoHp = (TextView) findViewById(R.id.tv_no_hp);
        txtAlamat = (TextView) findViewById(R.id.tv_alamat);
        tvTotal = (TextView) findViewById(R.id.tv_total_pembayaran);

        //ambil alamat
        Intent i = getIntent();
        if (i != null && i.getExtras() != null) {
            addresses = (AdressResponse.Addresses) i.getSerializableExtra("model");
            txtNama.setText(addresses.getName());
            txtNoHp.setText(addresses.getPhone());
            txtAlamat.setText(addresses.getAddress());
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.initData(getToken());

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkout(addresses.getId(),cartId );
            }
        });
    }

    private void checkout(Integer id, String cartId) {
        CheckoutForm checkoutForm = new CheckoutForm();
        checkoutForm.setAddress_id(id);
        checkoutForm.setDelivery_fee(0);
        checkoutForm.setCart_id(Integer.valueOf(cartId));
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<AddCheckoutResponse> movieModelCall = allService.addCheckout(checkoutForm, getToken());
        movieModelCall.enqueue(new Callback<AddCheckoutResponse>() {
            @Override
            public void onResponse(Call<AddCheckoutResponse> call, Response<AddCheckoutResponse> response) {
                if (response.isSuccessful()){
                    dialogCheckout(response.body().getMessage());
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<AddCheckoutResponse> call, Throwable t) {

            }
        });
    }

    private void dialogCheckout(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent i = new Intent(AddCheckoutActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void setButtonText(String total, String id){
        if (tvTotal == null) return;
        tvTotal.setText(getRupiahFormat(total));
        cartId = id;
    }

    public void setCartKosong(){
        cartId="";
        if (txtCartKosong == null) return;
        txtCartKosong.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
