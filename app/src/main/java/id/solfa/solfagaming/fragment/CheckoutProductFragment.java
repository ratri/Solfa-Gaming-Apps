package id.solfa.solfagaming.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import id.solfa.solfagaming.R;
import id.solfa.solfagaming.adapter.CheckoutProductAdapter;
import id.solfa.solfagaming.model.response.CheckoutResponse;

/**
 * Created by Ratri on 10/21/2017.
 */

public class CheckoutProductFragment extends Fragment {
    CheckoutResponse.Checkout checkout;
    RecyclerView recyclerView;
    CheckoutProductAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_checkout, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_cart);
        adapter = new CheckoutProductAdapter(getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.initData(checkout);
        return  view;
    }

    public static CheckoutProductFragment Instance(CheckoutResponse.Checkout checkout) {
        CheckoutProductFragment fragment = new CheckoutProductFragment();
        fragment.checkout = checkout;
        return  fragment;
    }
}
