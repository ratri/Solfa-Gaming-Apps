package id.solfa.solfagaming.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import id.solfa.solfagaming.BaseActivity;
import id.solfa.solfagaming.R;
import id.solfa.solfagaming.model.response.CheckoutResponse;

/**
 * Created by Ratri on 10/21/2017.
 */

public class CheckoutDetailFragment extends Fragment {
    CheckoutResponse.Checkout checkout;
    TextView tvTotalBelanja, tvTotalPembayaran,tvOngkir, tvAlamat, tvNama,tvNoHp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_transaksi, container, false);
        tvTotalBelanja = (TextView) view.findViewById(R.id.tv_total_belanja);
        tvTotalPembayaran= (TextView) view.findViewById(R.id.tv_total_pembayaran);
        tvOngkir = (TextView) view.findViewById(R.id.tv_ongkir);
//        tvAlamat = (TextView) view.findViewById(R.id.tv_alamat);
//        tvNama = (TextView) view.findViewById(R.id.tv_nama);
//        tvNoHp = (TextView) view.findViewById(R.id.tv_no_hp);

        tvTotalBelanja.setText(getRupiahFormat(""+checkout.getSubtotal()));
        tvTotalPembayaran.setText(getRupiahFormat(""+(checkout.getSubtotal()+checkout.getDelivery_fee())));
        tvOngkir.setText(getRupiahFormat(""+(checkout.getDelivery_fee())));

//        tvNama.setText(checkout.getAddress().getName());
//        tvNoHp.setText(checkout.getAddress().getPhone());
//        tvAlamat.setText(checkout.getAddress().getAddress());
        return  view;
    }

    public static CheckoutDetailFragment Instance( CheckoutResponse.Checkout checkout) {
        CheckoutDetailFragment fragment = new CheckoutDetailFragment();
        fragment.checkout=checkout;
        return fragment ;
    }

    public  String getRupiahFormat (String i){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###,###");
        String formattedString = formatter.format(Double.parseDouble(i));
        formattedString = formattedString.replaceAll(",",".");
        return "IDR "+formattedString;
    }
}
