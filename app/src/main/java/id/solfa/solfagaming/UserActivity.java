package id.solfa.solfagaming;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Ratri on 10/11/2017.
 */

public class UserActivity extends BaseActivity  implements View.OnClickListener{
    TextView tvAkun, tvBantuan, tvBerita, tvPembelian, tvSubcribe;
    SharedPreferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Akun Saya");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tvBantuan = (TextView) findViewById(R.id.bantuan);
        tvSubcribe = (TextView) findViewById(R.id.subcribe) ;
        tvPembelian = (TextView) findViewById(R.id.pembelian);

        tvBantuan.setOnClickListener(this);
        tvSubcribe.setOnClickListener(this);
        tvPembelian.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent i ;
        switch (view.getId()){
            case R.id.bantuan:
                i = new Intent(UserActivity.this, HelpActivity.class);
                startActivity(i);
                break;
            case R.id.subcribe:
                i = new Intent(UserActivity.this, SubcribeActivity.class);
                startActivity(i);
                break;
            case  R.id.pembelian:
                 i =new Intent(UserActivity.this, CheckoutActivity.class);
                startActivity(i);
                break;
        }
    }

    //fungsi logout
    public void logout (){
        pref = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(BaseActivity.IS_LOGGED_IN,false);
        editor.putString(BaseActivity.EMAIL,"");
        editor.putString(BaseActivity.NAME,"");
        editor.apply();
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Kamu berhasil logout")
                .show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
