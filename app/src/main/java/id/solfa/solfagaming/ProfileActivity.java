package id.solfa.solfagaming;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;

import com.google.gson.Gson;

import id.solfa.solfagaming.model.response.ProfilResponse;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/25/2017.
 */

public class ProfileActivity extends BaseActivity {
    EditText etEmail, etNama, etSteam;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etEmail = (EditText) findViewById(R.id.et_email);
        etNama = (EditText) findViewById(R.id.et_nama);
        etSteam = (EditText) findViewById(R.id.et_steam);

        initDataProfil(getToken());
    }

    public void initDataProfil(String token){
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<ProfilResponse> movieModelCall = allService.getProfil(token);
        movieModelCall.enqueue(new Callback<ProfilResponse>() {

            @Override
            public void onResponse(Call<ProfilResponse> call, Response<ProfilResponse> response) {
                if (response.isSuccessful()){
                    ProfilResponse.User resp = response.body().getUser();
                    etNama.setText(resp.getName());
                    etEmail.setText(resp.getEmail());
//                    etSteam.setText(resp.get);
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<ProfilResponse> call, Throwable t) {
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
