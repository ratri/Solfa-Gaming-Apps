package id.solfa.solfagaming;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Ratri on 11/26/2017.
 */

public class PaymentFragment extends Fragment {

    Button btnKonfirmasi;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        btnKonfirmasi = (Button) view.findViewById(R.id.btn_konfirmasi);
        btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(getActivity(), KonfirmasiPembayaranActivity.class);
                startActivity(i);
            }
        });

        return view;
    }
}
