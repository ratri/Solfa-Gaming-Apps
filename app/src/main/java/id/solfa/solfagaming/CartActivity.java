package id.solfa.solfagaming;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import id.solfa.solfagaming.adapter.CartAdapter;

/**
 * Created by Ratri on 10/10/2017.
 */

public class CartActivity extends BaseActivity {
    RecyclerView recyclerView;
    CartAdapter adapter;
    public Button btnTotal, btnCheckout;
    TextView txtCartKosong;
    int cart ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Keranjang Belanja");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        adapter = new CartAdapter(this, getToken());
        recyclerView = (RecyclerView) findViewById(R.id.rv_cart);
        txtCartKosong = (TextView) findViewById(R.id.cart_kosong) ;
        btnTotal = (Button) findViewById(R.id.btn_total) ;
        btnCheckout = (Button) findViewById(R.id.btn_checkout) ;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.initData(getToken());
        btnTotal.setText("TOTAL IDR " );
        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cart==0){
                    System.out.println("cart : " + cart);
                    dialogCartKosong("Keranjang belanja kosong.");
                } else {
                    System.out.println("cart : " + cart);
                    Intent i = new Intent(CartActivity.this, PilihAlamatActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    public void dialogCartKosong(String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setMessage(s);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void setButtonText(String total){
        if (Integer.valueOf(total)>0){
            cart = 1;
        } else {
            cart = 0;
        }

        if (btnTotal == null) return;
        btnTotal.setText("TOTAL "+getRupiahFormat(total));
    }

    public void setCartKosong(){
        if (txtCartKosong == null) return;
        txtCartKosong.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
