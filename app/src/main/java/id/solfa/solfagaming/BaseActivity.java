package id.solfa.solfagaming;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Ratri on 10/10/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public static final String LOGIN_OPERATION = "login_account";
    public static final String IS_LOGGED_IN = "isLoggedIn";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String ID = "id";
    public static final String TOKEN = "token";
    public int cartCount;
    public SharedPreferences pref;
    public static BaseActivity baseActivity;
    public  static  final  String grant_type = "password";
    public  static  final  String client_id = "2";
    public  static  final  String client_secret = "jtENSd6fEJ5k9PFoGHTfWHc5Kqn8i8IzG4fp1DHm";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static BaseActivity getBaseActivity (){
        return baseActivity;
    }

    public String getToken (){
        pref = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
        return pref.getString(TOKEN, "");
    }

    public boolean isLogin(){
        pref = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
        return pref.getBoolean(BaseActivity.IS_LOGGED_IN, false);
    }

    public  String getRupiahFormat (String i){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###,###");
        String formattedString = formatter.format(Double.parseDouble(i));
        formattedString = formattedString.replaceAll(",",".");
        return "IDR "+formattedString;
    }

    public void logout (){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(BaseActivity.IS_LOGGED_IN,false);
        editor.putString(BaseActivity.EMAIL,"");
        editor.putString(BaseActivity.NAME,"");
        editor.putString(BaseActivity.TOKEN,"");
        editor.apply();
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Kamu berhasil logout")
                .show();
    }

    public void lineAt (){
        String url = "http://line.me/R/ti/p/%40solfagaming";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

}
