package id.solfa.solfagaming;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import id.solfa.solfagaming.model.form.LoginFacebookForm;
import id.solfa.solfagaming.model.form.LoginForm;
import id.solfa.solfagaming.model.form.RegisterForm;
import id.solfa.solfagaming.model.response.LoginResponse;
import id.solfa.solfagaming.model.response.ProfilResponse;
import id.solfa.solfagaming.model.response.ServerResponse;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/22/2017.
 */

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    CallbackManager callbackManager;
    AppCompatButton btnLogin, btnFacebook;
    private ProgressDialog mProgressDialog;
    private static ProgressDialog mDialog;
    private Dialog fbDialog;
    private AppCompatButton btn_login, btn_logout;
    private EditText etEmail, etNama, etPassword;
    private TextView tv_register,tv_reset_password;
    private ProgressBar progress;
    private SharedPreferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Register");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

/**
 * facebook sdk
 */
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        btnFacebook = (AppCompatButton) findViewById(R.id.btn_facebook) ;
        btnFacebook.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Login Facebook Error")
                .setTitle("Error");
        final AlertDialog dialog = builder.create();

        mDialog = new ProgressDialog(this);
        fbDialog = new Dialog(this);
        fbDialog.setTitle("Error Login Facebook");

        // Other app specific specializatio
        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());
                                // Application code

                                try {
                                    String oldFormat= "MM/dd/yyyy";
                                    String newFormat= "dd-MM-yyyy";

                                    String formatedDate = "";
                                    SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
                                    Date myDate = null;

                                    String birthday;
                                    String gender;
                                    String email;
                                    String id = object.getString("id");
                                    String nama = object.getString("name");
                                    if(object.has("birthday")){
                                        birthday = object.getString("birthday"); // 01/31/1980 format
                                        try {
                                            myDate = dateFormat.parse(birthday);
                                        } catch (java.text.ParseException e) {
                                            e.printStackTrace();
                                        }
                                        SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
                                        birthday = timeFormat.format(myDate);
                                    } else {
                                        birthday="";
                                    }
                                    if(object.has("gender")){
                                        gender = object.getString("gender");
                                    } else {
                                        gender="";
                                    }
                                    if (object.has("email")){
                                        email = object.getString("email");
                                    } else {
                                        email ="";
                                        dialog.show();
                                    }

                                    String photo_url = "http://graph.facebook.com/"+id+"/picture?type=large";
                                    Log.i("url",photo_url);
                                    Log.d("fb", email);
                                    Log.d("fb", birthday);
//                                    Log.d("fb", publicProf);
                                    Log.d("fb", "login");

                                    if (gender.matches("female")){
                                        gender="Wanita";
                                    } else {
                                        gender="Pria";
                                    }
                                    Log.d("fb", gender+ formatedDate);

                                    if (!email.matches("")){
                                        Log.i("fb", "email kosong");
                                        getUrlFb(id, email, nama, gender,photo_url, birthday );
                                    }

                                    System.out.println("token fb "+ loginResult.getAccessToken().getToken());

                                    LoginFacebookForm loginForm = new LoginFacebookForm();
                                    loginForm.setClient_id(baseActivity.client_id);
                                    loginForm.setClient_secret(baseActivity.client_secret);
                                    loginForm.setGrant_type("facebook_login");
                                    loginForm.setScope("");
                                    loginForm.setFb_token(loginResult.getAccessToken().getToken());
                                    initFacebook(loginForm);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                //cek login fb


                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        btn_login = (AppCompatButton)findViewById(R.id.btn_register);
        tv_register = (TextView)findViewById(R.id.tv_register);
        etEmail = (EditText)findViewById(R.id.et_email);
        etPassword = (EditText)findViewById(R.id.et_password);
        etNama = (EditText)findViewById(R.id.et_nama);
        pref = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
        etEmail.setText(pref.getString(BaseActivity.EMAIL, ""));
        progress = (ProgressBar)findViewById(R.id.progress);
        btn_login.setOnClickListener(this);
        tv_register.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_facebook:
                loginFacebook();
                break;
            case R.id.btn_register:
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                String nama = etNama.getText().toString();

                if(!email.isEmpty() && !password.isEmpty() && !nama.isEmpty()) {
                    initDataRegister(email,password, nama);
                } else {
                    Snackbar.make(findViewById(R.id.tv_register), "Fields are empty !", Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.tv_register:
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
        }
    }

    /**
     * set model register
     * @param email
     * @param password
     * @param nama
     */
    private void initDataRegister(String email, String password, String nama){
        RegisterForm form = new RegisterForm();
        form.setEmail(email);
        form.setPassword(password);
        form.setName(nama);
        register(form);
    }

    /**
     * Register
     * @param form
     */
    public void register(RegisterForm form){
        showProgressDialog();
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<ServerResponse> loginResponseCall = allService.register(form);
        loginResponseCall.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                ServerResponse resp = response.body();
                System.out.println(response.toString());
                System.out.println(response.raw().toString());
                if(response.isSuccessful()){
                    System.out.println("....................sukses");
                    System.out.println(response.raw().toString());
                    System.out.println(new Gson().toJson(response.body()));
                    dialogRegister(response.body().getMessage());
                }
                else {
                    System.out.println("....................else login");
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                System.out.println("...................."+t.toString());
            }
        });
    }

    private void dialogRegister(String message) {
        AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                onBackPressed();
            }
        });
        AlertDialog alert = builder.create();
        alert.setTitle("Sukses");
        alert.setMessage(message);
        alert.setCancelMessage(null);
        alert.show();
    }

    public void initFacebook(LoginFacebookForm loginFacebookForm){
        showProgressDialog();
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<LoginResponse> loginResponseCall = allService.loginFacebook(loginFacebookForm);
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse resp = response.body();
                System.out.println(response.toString());
                System.out.println(response.raw().toString());
                //if(resp.getResult().equals(BaseActivity.SUCCESS)){
                if(response.isSuccessful()){
//                        //Toast.makeText(, resp.getMessage(), 6);

                    System.out.println("....................sukses");
                    System.out.println(response.raw().toString());
                    System.out.println(new Gson().toJson(response.body()));
                    System.out.println(resp.getToken_type() + " " +resp.getAccess_token());
                    //System.out.println(resp.getResult()+ resp.getMessage());


                    pref = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(BaseActivity.IS_LOGGED_IN,true);
                    editor.putString(BaseActivity.TOKEN, resp.getToken_type() + " " +resp.getAccess_token());
                    editor.apply();
                    initDataProfil(resp.getToken_type() + " " +resp.getAccess_token());
                    Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }
                else {
                    System.out.println("....................else login");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                System.out.println("...................."+t.toString());
            }
        });
    }

    public void initDataProfil(String token){
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<ProfilResponse> movieModelCall = allService.getProfil(token);
        movieModelCall.enqueue(new Callback<ProfilResponse>() {

            @Override
            public void onResponse(Call<ProfilResponse> call, Response<ProfilResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    ProfilResponse.User resp = response.body().getUser();

                    System.out.println(new Gson().toJson(response.body()));
                    System.out.println(response.body());

                    pref = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(BaseActivity.EMAIL, resp.getEmail());
                    editor.putString(BaseActivity.NAME, resp.getName());
                    editor.putString(BaseActivity.ID, resp.getId());
                    editor.apply();
                    Log.d("nama", resp.getName());

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<ProfilResponse> call, Throwable t) {
                hideProgressDialog();

            }
        });
    }


    private void loginFacebook() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null){ //belum login fb
            Log.d("fb", "ada");
            LoginManager.getInstance().logOut();
        }
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday", "user_photos"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //fb
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading ..");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void getUrlFb(final String id, final String email, final String nama, final String gender, final String photo_url, final String formatedDate){
//        mDialog.show();
//        RestApiService service = Api.getInstance().createServiceFacebook(RestApiService.class, GeneralDeserialization.converterFactory(), id );
//        Call<FacebookModel> call = service.getUrlFb("small", "false", "400", "400");
//        call.enqueue(new Callback<FacebookModel>() {
//
//            @Override
//            public void onResponse(Call<FacebookModel> call, Response<FacebookModel> response) {
//                url = response.body().getData().getUrl();
//                Log.i("url", url);
//                inputLoginData( email, nama, gender,url, formatedDate  );
//            }
//            @Override
//            public void onFailure(Call<FacebookModel> call, Throwable t) {
//                inputLoginData(email, nama, gender,"", formatedDate  );
//            }
//        });
//        mDialog.dismiss();

    }


}
