package id.solfa.solfagaming.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.solfa.solfagaming.R;

/**
 * Created by Ratri on 8/24/2017.
 */

public class AdressHolder extends  RecyclerView.ViewHolder {
    public TextView txtNama, txtEmail, txtNoHp, txtAlamat;
public AdressHolder(View itemView) {
        super(itemView);
        txtNama = (TextView) itemView.findViewById(R.id.text_nama);
        txtEmail = (TextView) itemView.findViewById(R.id.text_email);
        txtNoHp = (TextView) itemView.findViewById(R.id.text_no_hp);
        txtAlamat = (TextView) itemView.findViewById(R.id.text_alamat);

        }

public AdressHolder(ViewGroup parrent) {
        this(LayoutInflater.from(parrent.getContext()).inflate(R.layout.item_adress, parrent, false));
        }
        }
