package id.solfa.solfagaming.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.solfa.solfagaming.R;

/**
 * Created by Ratri on 10/25/2017.
 */

public class AddCheckoutHolder extends RecyclerView.ViewHolder {
    public TextView tvNama, tvHarga, tvJumlah, tvSubtotal;
    public ImageView cover;
    public LinearLayout lin;
    public CardView cardView;

    public AddCheckoutHolder(View itemView) {
        super(itemView);
        tvNama = (TextView) itemView.findViewById(R.id.nama);
        tvHarga = (TextView) itemView.findViewById(R.id.harga);
        cover = (ImageView) itemView.findViewById(R.id.thumbnail);
//        delete = (ImageView) itemView.findViewById(R.id.delete);
        lin = (LinearLayout) itemView.findViewById(R.id.lin);
        tvJumlah = (TextView) itemView.findViewById(R.id.jumlah);
        tvSubtotal = (TextView) itemView.findViewById(R.id.subtotal);
        System.out.println("itemPricelist");

    }

    public AddCheckoutHolder(ViewGroup parrent) {
        this(LayoutInflater.from(parrent.getContext()).inflate(R.layout.item_add_checkout, parrent, false));
    }
}