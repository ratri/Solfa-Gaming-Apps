package id.solfa.solfagaming.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.solfa.solfagaming.R;

/**
 * Created by Ratri on 10/21/2017.
 */

public class CheckoutHolder extends RecyclerView.ViewHolder {
    public TextView tvInvoice, tvNominal, tvStatus, tvTanggal, tvNama, tvJumlah, tvJumlahTotal,tvHarga;
    public  ImageView imgProduk;

    public CheckoutHolder (View itemView) {
        super(itemView);
        tvInvoice = (TextView) itemView.findViewById(R.id.tv_invoice);
        tvNominal = (TextView)itemView.findViewById(R.id.tv_nominal);
        tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
        tvTanggal = (TextView) itemView.findViewById(R.id.tv_tanggal);
        tvNama = (TextView) itemView.findViewById(R.id.tv_nama);
        tvJumlah = (TextView) itemView.findViewById(R.id.tv_jumlah);
        tvJumlahTotal = (TextView) itemView.findViewById(R.id.tv_jumlah_total);
        tvHarga = (TextView) itemView.findViewById(R.id.tv_harga);
        imgProduk = (ImageView) itemView.findViewById(R.id.img_produk);
    }

    public CheckoutHolder (ViewGroup parrent) {
        this(LayoutInflater.from(parrent.getContext()).inflate(R.layout.item_transaksi, parrent, false));
    }
}