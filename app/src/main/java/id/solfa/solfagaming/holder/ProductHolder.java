package id.solfa.solfagaming.holder;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.solfa.solfagaming.R;

/**
 * Created by Ratri on 10/24/2017.
 */

public class ProductHolder extends RecyclerView.ViewHolder {
    public TextView txt_nama, txt_harga;
    public ImageView cover, cart;
    public LinearLayout lin;
    public CardView cardView;

    public ProductHolder(View itemView) {
        super(itemView);
        txt_nama = (TextView) itemView.findViewById(R.id.nama);
        txt_harga = (TextView) itemView.findViewById(R.id.harga);
        cover = (ImageView) itemView.findViewById(R.id.thumbnail);
        cart = (ImageView) itemView.findViewById(R.id.cart);
        lin = (LinearLayout) itemView.findViewById(R.id.lin);
    }

    public ProductHolder(ViewGroup parrent) {
        this(LayoutInflater.from(parrent.getContext()).inflate(R.layout.item_produk, parrent, false));

    }

}
