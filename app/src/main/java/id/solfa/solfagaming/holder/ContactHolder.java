package id.solfa.solfagaming.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import id.solfa.solfagaming.R;

/**
 * Created by Ratri on 10/10/2017.
 */

public class ContactHolder extends  RecyclerView.ViewHolder {
public ImageView imgContact;

    public ContactHolder(View itemView) {
        super(itemView);
        imgContact = (ImageView) itemView.findViewById(R.id.img_contact);
    }

    public ContactHolder(ViewGroup parrent) {
        this(LayoutInflater.from(parrent.getContext()).inflate(R.layout.item_contact, parrent, false));
    }
}