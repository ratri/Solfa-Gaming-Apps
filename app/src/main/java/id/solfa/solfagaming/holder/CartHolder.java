package id.solfa.solfagaming.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.solfa.solfagaming.R;

/**
 * Created by Ratri on 10/10/2017.
 */

public class CartHolder extends RecyclerView.ViewHolder {
    public TextView txt_nama, txt_harga, txt_jumlah, tvMin, tvPlus;
    public ImageView cover;
    public LinearLayout lin;
    public CardView cardView;

    public CartHolder(View itemView) {
        super(itemView);
        txt_nama = (TextView) itemView.findViewById(R.id.nama);
        txt_harga = (TextView) itemView.findViewById(R.id.harga);
        cover = (ImageView) itemView.findViewById(R.id.thumbnail);
//        delete = (ImageView) itemView.findViewById(R.id.delete);
        lin = (LinearLayout) itemView.findViewById(R.id.lin);
        txt_jumlah = (TextView) itemView.findViewById(R.id.jumlah);
        tvMin = (TextView) itemView.findViewById(R.id.tv_min);
        tvPlus = (TextView) itemView.findViewById(R.id.tv_plus);

        System.out.println("itemPricelist");

    }

    public CartHolder(ViewGroup parrent) {
        this(LayoutInflater.from(parrent.getContext()).inflate(R.layout.item_cart, parrent, false));
    }
}