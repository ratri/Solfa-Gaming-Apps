package id.solfa.solfagaming.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.solfa.solfagaming.R;

/**
 * Created by Ratri on 10/22/2017.
 */

public class CheckoutProductHolder  extends RecyclerView.ViewHolder  {
    public TextView txt_nama, txt_harga, txt_jumlah;
    public ImageView cover;
    public LinearLayout lin;
    public CardView cardView;

    public CheckoutProductHolder(View itemView) {
        super(itemView);
        txt_nama = (TextView) itemView.findViewById(R.id.nama);
        txt_harga = (TextView) itemView.findViewById(R.id.harga);
        cover = (ImageView) itemView.findViewById(R.id.thumbnail);
        lin = (LinearLayout) itemView.findViewById(R.id.lin);
        txt_jumlah = (TextView) itemView.findViewById(R.id.jumlah);

        System.out.println("itemPricelist");

    }

    public CheckoutProductHolder(ViewGroup parrent) {
        this(LayoutInflater.from(parrent.getContext()).inflate(R.layout.item_checkout_product, parrent, false));
    }
}