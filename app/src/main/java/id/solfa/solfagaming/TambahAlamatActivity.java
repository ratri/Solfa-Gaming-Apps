package id.solfa.solfagaming;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import id.solfa.solfagaming.model.form.AddressForm;
import id.solfa.solfagaming.model.response.AddressResponse;
import id.solfa.solfagaming.model.response.CityResponse;
import id.solfa.solfagaming.model.response.DistrictResponse;
import id.solfa.solfagaming.model.response.ProvinsiResponse;
import id.solfa.solfagaming.model.response.SubDistrictResponse;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/15/2017.
 */

public class TambahAlamatActivity extends BaseActivity implements AdapterView.OnItemSelectedListener{
    Spinner spProvinsi, spKota, spKecamatan, spDesa;
    ArrayList <String> arrayProv = new ArrayList<>();
    ArrayList <String> arrayKecamatan = new ArrayList<>();
    ArrayList <String> arrayDesa = new ArrayList<>();
    ArrayList <String> arrayKota = new ArrayList<>();

    ProvinsiResponse provinsiResponses;
    CityResponse cityResponses =new CityResponse();
    DistrictResponse districtResponses = new DistrictResponse();
    SubDistrictResponse subDistrictResponse = new SubDistrictResponse();
    ArrayAdapter <CharSequence> adapterProv;
    ArrayAdapter <CharSequence> adapterKota;
    ArrayAdapter <CharSequence> adapterKecamatan;
    ArrayAdapter <CharSequence> adapterDesa;
    AddressForm addressForm;
    EditText etNama, etALamat, etTelepon;
    Button btnSimpan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_alamat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Tambah Alamat");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //model alamat
        addressForm = new AddressForm();

        spProvinsi = (Spinner) findViewById(R.id.sp_provinsi);
        spKota = (Spinner) findViewById(R.id.sp_kota);
        spKecamatan = (Spinner) findViewById(R.id.sp_kecamatan);
        spDesa = (Spinner) findViewById(R.id.sp_desa);
        etNama = (EditText) findViewById(R.id.et_nama);
        etALamat = (EditText) findViewById(R.id.et_alamat);
        etTelepon = (EditText) findViewById(R.id.et_telepon);
        btnSimpan = (Button) findViewById(R.id.btn_simpan);

        //spinner

        arrayProv.add("- Pilih Provinsi -");
        arrayKota.add("- Pilih Kota/Kab -");
        arrayKecamatan.add("- Pilih Kecamatan -");
        arrayDesa.add("- Pilih Desa -");

        initDataProvinsi();
        spinnerDesa();
        spinnerKecamatan();
        spinnerKota();

        spProvinsi.setOnItemSelectedListener(this);
        spKota.setOnItemSelectedListener(this);
        spKecamatan.setOnItemSelectedListener(this);
        spDesa.setOnItemSelectedListener(this);

        //simpan
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addressForm.setName(etNama.getText().toString());
                addressForm.setPhone(etTelepon.getText().toString());
                addressForm.setAddress(etALamat.getText().toString());
                pref = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
                addressForm.setEmail(pref.getString(EMAIL, ""));

                if (!addressForm.getName().equals("")&&!addressForm.getPhone().equals("")&&!addressForm.getAddress().equals("")
                        &&!addressForm.getSubdistrict_id().equals("")){
                    tambahAlamat(addressForm);
                } else {
                    toast("Isi harus lengkap");
                }
            }
        });

    }

    private void tambahAlamat(AddressForm addressForm) {
        AllService service = ServiceGenerator.connect(AllService.class);
        Call<AddressResponse> movieModelCall =service.addAddress(addressForm, getToken());
        movieModelCall.enqueue(new Callback<AddressResponse>() {

            @Override
            public void onResponse(Call<AddressResponse> call, Response<AddressResponse> response) {
                System.out.println("resp"+response.message().toString());
                System.out.println("resp"+new Gson().toJson(response.body()));
                System.out.println("resp"+new Gson().toJson(response.message()));

                if (response.isSuccessful()){
                   dialog(response.message());
                } else Log.e("ERROR", "not success : "+response.raw());

                }

            @Override
            public void onFailure(Call<AddressResponse> call, Throwable t) {
                System.out.println("fail"+ t);
                toast("t");
            }
        });
    }

    private void dialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                onBackPressed();
            }
        });
        builder.setMessage(message);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void toast(String toast){
        Toast.makeText(this, toast, Toast.LENGTH_LONG);
    }

    public void initDataProvinsi(){
        System.out.println("init aderess");
        AllService service = ServiceGenerator.connect(AllService.class);
        Call<ProvinsiResponse> movieModelCall =service.getProvinces();
        movieModelCall.enqueue(new Callback<ProvinsiResponse>() {

            @Override
            public void onResponse(Call<ProvinsiResponse> call, Response<ProvinsiResponse> response) {
                System.out.println("resp"+response.message().toString());
                System.out.println("resp"+new Gson().toJson(response.body()));
                System.out.println("resp"+new Gson().toJson(response.message()));
                System.out.println("resp"+ response.body().getMessage());

                if (response.isSuccessful()){
                    System.out.println("suces");
                    provinsiResponses = new ProvinsiResponse();
                    provinsiResponses = response.body();
                    arrayProv.clear();
                    arrayProv.add("- Pilih Provinsi -");
                    for (int i=0;i<response.body().getProvinces().size(); i++){
                        ProvinsiResponse.Province province = provinsiResponses.getProvinces().get(i);
                        arrayProv.add(province.getName());
                    }

                    spinnerProv();
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<ProvinsiResponse> call, Throwable t) {
                System.out.println("fail"+ t);
            }
        });
    }

    private void spinnerProv() {
        adapterProv = new ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayProv );
        adapterProv.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spProvinsi.setAdapter(adapterProv);
    }

    private void spinnerKota (){
        adapterKota= new ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayKota );
        adapterKota.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spKota.setAdapter(adapterKota);
    }

    private void spinnerDesa(){
        adapterDesa= new ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayDesa );
        adapterDesa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDesa.setAdapter(adapterDesa);
    }

    private void spinnerKecamatan(){
        adapterKecamatan= new ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayKecamatan );
        adapterKecamatan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spKecamatan.setAdapter(adapterKecamatan);
    }



    public void initDataKota(String id){
        System.out.println("init aderess");
        AllService service = ServiceGenerator.connect(AllService.class);
        Call<CityResponse> modelCall =service.getCity(id);
        modelCall.enqueue(new Callback<CityResponse>() {

            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                System.out.println("resp"+response.message().toString());
                System.out.println("resp"+new Gson().toJson(response.body()));
                System.out.println("resp"+new Gson().toJson(response.message()));
                System.out.println("resp"+ response.body().getMessage());

                if (response.isSuccessful()){
                    cityResponses = new CityResponse();
                    cityResponses = response.body();
                    arrayKota.clear();
                    arrayKota.add("- Pilih Kota/Kab -");
                    for (int i=0; i<cityResponses.getCities().size(); i++){
                        arrayKota.add(cityResponses.getCities().get(i).getName());
                    }
                   spinnerKota();
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                System.out.println("fail"+ t);
            }
        });
    }

    public void initDataKecamatan(String id){
        System.out.println("init aderess");
        AllService service = ServiceGenerator.connect(AllService.class);
        Call<DistrictResponse> modelCall =service.getDistrict(id);
        modelCall.enqueue(new Callback<DistrictResponse>() {

            @Override
            public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                System.out.println("resp"+response.message().toString());
                System.out.println("resp"+new Gson().toJson(response.body()));
                System.out.println("resp"+new Gson().toJson(response.message()));
                System.out.println("resp"+ response.body().getMessage());

                if (response.isSuccessful()){
                    System.out.println("suces");
                    districtResponses = response.body();
                    arrayKecamatan.clear();
                    arrayKecamatan.add("- Pilih Kecamatan -");
                    for (int i=0; i<districtResponses.getDistricts().size();i++){
                        arrayKecamatan.add(districtResponses.getDistricts().get(i).getName());
                    }
                    spinnerKecamatan();
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<DistrictResponse> call, Throwable t) {
                System.out.println("fail"+ t);
            }
        });
    }

    public void initDataDesa(String id){
        System.out.println("id kec " + id);
        AllService service = ServiceGenerator.connect(AllService.class);
        Call<SubDistrictResponse> modelCall =service.getSubDistrict(id);
        modelCall.enqueue(new Callback<SubDistrictResponse>() {

            @Override
            public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                System.out.println("resp"+response.message().toString());
                System.out.println("resp"+new Gson().toJson(response.body()));
                System.out.println("resp"+new Gson().toJson(response.message()));
                System.out.println("resp"+ response.body().getMessage());

                if (response.isSuccessful()){
                    System.out.println("suces");
                    subDistrictResponse =  response.body();
                    arrayDesa.clear();
                    arrayDesa.add("- Pilih Desa -");
                    for (int i = 0; i<subDistrictResponse.getSubdistricts().size();i++){
                        arrayDesa.add(subDistrictResponse.getSubdistricts().get(i).getName());
                    }
                    spinnerDesa();
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                System.out.println("fail"+ t);
            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        int position = parent.getSelectedItemPosition()-1;
        if (position >= 0) {
            switch (parent.getId()){
                case R.id.sp_provinsi:
                    if (provinsiResponses.getProvinces().size()>0) {
                            initDataKota(provinsiResponses.getProvinces().get(position).getId());
                    }
                    break;
                case R.id.sp_kota:
                    if (cityResponses.getCities().size()>0){
                        initDataKecamatan(cityResponses.getCities().get(position).getId());
                    }
                    break;
                case R.id.sp_kecamatan:
                    if (districtResponses.getDistricts().size()>0){
                        initDataDesa(districtResponses.getDistricts().get(position).getId());
                    }
                    break;
                case R.id.sp_desa:
                    if (subDistrictResponse.getSubdistricts().size()>0){
                        addressForm.setSubdistrict_id(""+subDistrictResponse.getSubdistricts().get(position).getId());
                    }
                    break;
            }
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
