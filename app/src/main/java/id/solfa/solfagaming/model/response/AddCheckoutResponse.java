package id.solfa.solfagaming.model.response;

/**
 * Created by Ratri on 10/12/2017.
 */

public class AddCheckoutResponse {
    String error, message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
