package id.solfa.solfagaming.model.response;

/**
 * Created by Ratri on 8/20/2017.
 */

public class UpdateCartResponse {
    String error, response;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
