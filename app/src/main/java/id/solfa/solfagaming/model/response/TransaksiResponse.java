package id.solfa.solfagaming.model.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ratri on 10/12/2017.
 */

public class TransaksiResponse {
        private String message;
        private Boolean error;

        public String getMessage() {
                return message;
        }

        public void setMessage(String message) {
                this.message = message;
        }

        public Boolean getError() {
                return error;
        }

        public void setError(Boolean error) {
                this.error = error;
        }

        public List<Checkout> getCheckouts() {
                return checkouts;
        }

        public void setCheckouts(List<Checkout> checkouts) {
                this.checkouts = checkouts;
        }

        private List<Checkout> checkouts = new ArrayList<>();

        public class Checkout {

                private Integer id;
                private Integer cart_id;
                private Integer user_id;
                private Integer address_id;
                private String invoice_no;
                private Integer delivery_fee;
                private Object note;
                private Integer checkout_state;
                private String created_at;
                private String updated_at;
                private Cart cart;
                private Address address;
                private User user;


                public Integer getId() {
                        return id;
                }

                public void setId(Integer id) {
                        this.id = id;
                }

                public Integer getCart_id() {
                        return cart_id;
                }

                public void setCart_id(Integer cart_id) {
                        this.cart_id = cart_id;
                }

                public Integer getUser_id() {
                        return user_id;
                }

                public void setUser_id(Integer user_id) {
                        this.user_id = user_id;
                }

                public Integer getAddress_id() {
                        return address_id;
                }

                public void setAddress_id(Integer address_id) {
                        this.address_id = address_id;
                }

                public String getInvoice_no() {
                        return invoice_no;
                }

                public void setInvoice_no(String invoice_no) {
                        this.invoice_no = invoice_no;
                }

                public Integer getDelivery_fee() {
                        return delivery_fee;
                }

                public void setDelivery_fee(Integer delivery_fee) {
                        this.delivery_fee = delivery_fee;
                }

                public Object getNote() {
                        return note;
                }

                public void setNote(Object note) {
                        this.note = note;
                }

                public Integer getCheckout_state() {
                        return checkout_state;
                }

                public void setCheckout_state(Integer checkout_state) {
                        this.checkout_state = checkout_state;
                }

                public String getCreated_at() {
                        return created_at;
                }

                public void setCreated_at(String created_at) {
                        this.created_at = created_at;
                }

                public String getUpdated_at() {
                        return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                        this.updated_at = updated_at;
                }

                public Cart getCart() {
                        return cart;
                }

                public void setCart(Cart cart) {
                        this.cart = cart;
                }

                public Address getAddress() {
                        return address;
                }

                public void setAddress(Address address) {
                        this.address = address;
                }

                public User getUser() {
                        return user;
                }

                public void setUser(User user) {
                        this.user = user;
                }

                public class Cart {

                        private Integer id;
                        private Integer user_id;
                        private Integer state;
                        private String created_at;
                        private String updated_at;
                        private String expired_at;
                        private List<Product> products = null;

                        public class Product {

                                private String product_id;
                                private String sku;
                                private String brand_id;
                                private String category_id;
                                private String name;
                                private String description;
                                private Integer weight;
                                private Integer price;
                                private Integer stock;
                                private Options options;
                                private Attributes attributes;
                                private List<Image> images = null;
                                private Integer state;
                                private Integer created_by;
                                private String created_at;
                                private String updated_at;
                                private Pivot pivot;

                                public class Attributes {

                                        private String ukuran;
                                        private String bahan;
                                        private String warna;

                                        public String getUkuran() {
                                                return ukuran;
                                        }

                                        public void setUkuran(String ukuran) {
                                                this.ukuran = ukuran;
                                        }

                                        public String getBahan() {
                                                return bahan;
                                        }

                                        public void setBahan(String bahan) {
                                                this.bahan = bahan;
                                        }

                                        public String getWarna() {
                                                return warna;
                                        }

                                        public void setWarna(String warna) {
                                                this.warna = warna;
                                        }

                                }

                                public class Image {

                                        private String uuid;
                                        private String title;
                                        private String photo;

                                        public String getUuid() {
                                                return uuid;
                                        }

                                        public void setUuid(String uuid) {
                                                this.uuid = uuid;
                                        }

                                        public String getTitle() {
                                                return title;
                                        }

                                        public void setTitle(String title) {
                                                this.title = title;
                                        }

                                        public String getPhoto() {
                                                return photo;
                                        }

                                        public void setPhoto(String photo) {
                                                this.photo = photo;
                                        }

                                }


                                public class Options {

                                        private String type;
                                        private String name;
                                        private List<Item> items = null;

                                        public class Item {

                                                private String name;
                                                private String value;
                                                private Integer weight;
                                                private Integer price;

                                                public String getName() {
                                                        return name;
                                                }

                                                public void setName(String name) {
                                                        this.name = name;
                                                }

                                                public String getValue() {
                                                        return value;
                                                }

                                                public void setValue(String value) {
                                                        this.value = value;
                                                }

                                                public Integer getWeight() {
                                                        return weight;
                                                }

                                                public void setWeight(Integer weight) {
                                                        this.weight = weight;
                                                }

                                                public Integer getPrice() {
                                                        return price;
                                                }

                                                public void setPrice(Integer price) {
                                                        this.price = price;
                                                }

                                        }

                                        public String getType() {
                                                return type;
                                        }

                                        public void setType(String type) {
                                                this.type = type;
                                        }

                                        public String getName() {
                                                return name;
                                        }

                                        public void setName(String name) {
                                                this.name = name;
                                        }

                                        public List<Item> getItems() {
                                                return items;
                                        }

                                        public void setItems(List<Item> items) {
                                                this.items = items;
                                        }

                                }



                                public String getProduct_id() {
                                        return product_id;
                                }

                                public void setProduct_id(String product_id) {
                                        this.product_id = product_id;
                                }

                                public String getSku() {
                                        return sku;
                                }

                                public void setSku(String sku) {
                                        this.sku = sku;
                                }

                                public String getBrand_id() {
                                        return brand_id;
                                }

                                public void setBrand_id(String brand_id) {
                                        this.brand_id = brand_id;
                                }

                                public String getCategory_id() {
                                        return category_id;
                                }

                                public void setCategory_id(String category_id) {
                                        this.category_id = category_id;
                                }

                                public String getName() {
                                        return name;
                                }

                                public void setName(String name) {
                                        this.name = name;
                                }

                                public String getDescription() {
                                        return description;
                                }

                                public void setDescription(String description) {
                                        this.description = description;
                                }

                                public Integer getWeight() {
                                        return weight;
                                }

                                public void setWeight(Integer weight) {
                                        this.weight = weight;
                                }

                                public Integer getPrice() {
                                        return price;
                                }

                                public void setPrice(Integer price) {
                                        this.price = price;
                                }

                                public Integer getStock() {
                                        return stock;
                                }

                                public void setStock(Integer stock) {
                                        this.stock = stock;
                                }

                                public Options getOptions() {
                                        return options;
                                }

                                public void setOptions(Options options) {
                                        this.options = options;
                                }

                                public Attributes getAttributes() {
                                        return attributes;
                                }

                                public void setAttributes(Attributes attributes) {
                                        this.attributes = attributes;
                                }

                                public List<Image> getImages() {
                                        return images;
                                }

                                public void setImages(List<Image> images) {
                                        this.images = images;
                                }

                                public Integer getState() {
                                        return state;
                                }

                                public void setState(Integer state) {
                                        this.state = state;
                                }

                                public Integer getCreated_by() {
                                        return created_by;
                                }

                                public void setCreated_by(Integer created_by) {
                                        this.created_by = created_by;
                                }

                                public String getCreated_at() {
                                        return created_at;
                                }

                                public void setCreated_at(String created_at) {
                                        this.created_at = created_at;
                                }

                                public String getUpdated_at() {
                                        return updated_at;
                                }

                                public void setUpdated_at(String updated_at) {
                                        this.updated_at = updated_at;
                                }

                                public Pivot getPivot() {
                                        return pivot;
                                }

                                public void setPivot(Pivot pivot) {
                                        this.pivot = pivot;
                                }

                                public class Pivot {

                                        private Integer cart_id;
                                        private String product_id;
                                        private Integer amount;
                                        private Integer price;

                                        public Integer getCart_id() {
                                                return cart_id;
                                        }

                                        public void setCart_id(Integer cart_id) {
                                                this.cart_id = cart_id;
                                        }

                                        public String getProduct_id() {
                                                return product_id;
                                        }

                                        public void setProduct_id(String product_id) {
                                                this.product_id = product_id;
                                        }

                                        public Integer getAmount() {
                                                return amount;
                                        }

                                        public void setAmount(Integer amount) {
                                                this.amount = amount;
                                        }

                                        public Integer getPrice() {
                                                return price;
                                        }

                                        public void setPrice(Integer price) {
                                                this.price = price;
                                        }

                                }

                        }

                        public Integer getId() {
                                return id;
                        }

                        public void setId(Integer id) {
                                this.id = id;
                        }

                        public Integer getUser_id() {
                                return user_id;
                        }

                        public void setUser_id(Integer user_id) {
                                this.user_id = user_id;
                        }

                        public Integer getState() {
                                return state;
                        }

                        public void setState(Integer state) {
                                this.state = state;
                        }

                        public String getCreated_at() {
                                return created_at;
                        }

                        public void setCreated_at(String created_at) {
                                this.created_at = created_at;
                        }

                        public String getUpdated_at() {
                                return updated_at;
                        }

                        public void setUpdated_at(String updated_at) {
                                this.updated_at = updated_at;
                        }

                        public String getExpired_at() {
                                return expired_at;
                        }

                        public void setExpired_at(String expired_at) {
                                this.expired_at = expired_at;
                        }

                        public List<Product> getProducts() {
                                return products;
                        }

                        public void setProducts(List<Product> products) {
                                this.products = products;
                        }

                }

                public class Address {

                        private Integer id;
                        private Integer user_id;
                        private String name;
                        private String email;
                        private String phone;
                        private String address;
                        private Integer subdistrict_id;
                        private Integer state;
                        private String created_at;
                        private String updated_at;

                        public Integer getId() {
                                return id;
                        }

                        public void setId(Integer id) {
                                this.id = id;
                        }

                        public Integer getUser_id() {
                                return user_id;
                        }

                        public void setUser_id(Integer user_id) {
                                this.user_id = user_id;
                        }

                        public String getName() {
                                return name;
                        }

                        public void setName(String name) {
                                this.name = name;
                        }

                        public String getEmail() {
                                return email;
                        }

                        public void setEmail(String email) {
                                this.email = email;
                        }

                        public String getPhone() {
                                return phone;
                        }

                        public void setPhone(String phone) {
                                this.phone = phone;
                        }

                        public String getAddress() {
                                return address;
                        }

                        public void setAddress(String address) {
                                this.address = address;
                        }

                        public Integer getSubdistrict_id() {
                                return subdistrict_id;
                        }

                        public void setSubdistrict_id(Integer subdistrict_id) {
                                this.subdistrict_id = subdistrict_id;
                        }

                        public Integer getState() {
                                return state;
                        }

                        public void setState(Integer state) {
                                this.state = state;
                        }

                        public String getCreated_at() {
                                return created_at;
                        }

                        public void setCreated_at(String created_at) {
                                this.created_at = created_at;
                        }

                        public String getUpdated_at() {
                                return updated_at;
                        }

                        public void setUpdated_at(String updated_at) {
                                this.updated_at = updated_at;
                        }

                }

                public class User {

                        private Integer id;
                        private String name;
                        private String email;
                        private Integer role;
                        private String state;
                        private String created_at;
                        private String updated_at;

                        public Integer getId() {
                                return id;
                        }

                        public void setId(Integer id) {
                                this.id = id;
                        }

                        public String getName() {
                                return name;
                        }

                        public void setName(String name) {
                                this.name = name;
                        }

                        public String getEmail() {
                                return email;
                        }

                        public void setEmail(String email) {
                                this.email = email;
                        }

                        public Integer getRole() {
                                return role;
                        }

                        public void setRole(Integer role) {
                                this.role = role;
                        }

                        public String getState() {
                                return state;
                        }

                        public void setState(String state) {
                                this.state = state;
                        }

                        public String getCreated_at() {
                                return created_at;
                        }

                        public void setCreated_at(String created_at) {
                                this.created_at = created_at;
                        }

                        public String getUpdated_at() {
                                return updated_at;
                        }

                        public void setUpdated_at(String updated_at) {
                                this.updated_at = updated_at;
                        }

                }

        }

}
