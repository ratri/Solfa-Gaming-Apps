package id.solfa.solfagaming.model.form;

/**
 * Created by Ratri on 8/20/2017.
 */

public class CartForm {
    String product_id;
    String amount;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


}
