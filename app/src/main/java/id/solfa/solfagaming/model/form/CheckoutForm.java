package id.solfa.solfagaming.model.form;

/**
 * Created by Ratri on 10/12/2017.
 */

public class CheckoutForm {
    private Integer cart_id;
    private Integer address_id;
    private Integer delivery_fee;

    public Integer getCart_id() {
        return cart_id;
    }

    public void setCart_id(Integer cart_id) {
        this.cart_id = cart_id;
    }

    public Integer getAddress_id() {
        return address_id;
    }

    public void setAddress_id(Integer address_id) {
        this.address_id = address_id;
    }

    public Integer getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(Integer delivery_fee) {
        this.delivery_fee = delivery_fee;
    }
}
