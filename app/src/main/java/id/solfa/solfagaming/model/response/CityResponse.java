package id.solfa.solfagaming.model.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ratri on 10/2/2017.
 */

public class CityResponse {
    private Boolean error;
    private String message;
    private List<City> cities = new ArrayList<>();

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public class City {

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        private String id;
        private Integer province_id;
        private String name;

        public Integer getProvince_id() {
            return province_id;
        }

        public void setProvince_id(Integer province_id) {
            this.province_id = province_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
