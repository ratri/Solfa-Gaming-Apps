package id.solfa.solfagaming.model.response;

import android.content.Intent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import id.solfa.solfagaming.AddCheckoutActivity;
import id.solfa.solfagaming.PilihAlamatActivity;

/**
 * Created by Ratri on 8/24/2017.
 */

public class AdressResponse {

    private String message;
    private Boolean error;
    private List<Addresses> addresses =new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<Addresses> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
    }

    public class Addresses implements Serializable{

        private Integer id;
        private Integer user_id;
        private String name;
        private String email;
        private String phone;
        private String address;
        private Integer subdistrict_id;
        private String state;
        private String created_at;
        private String updated_at;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUser_id() {
            return user_id;
        }

        public void setUser_id(Integer user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Integer getSubdistrict_id() {
            return subdistrict_id;
        }

        public void setSubdistrict_id(Integer subdistrict_id) {
            this.subdistrict_id = subdistrict_id;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public void checkout(PilihAlamatActivity activity) {
            Intent i = new Intent(activity, AddCheckoutActivity.class);
            i.putExtra("model", this);
            activity.startActivity(i);

        }
    }
}
