package id.solfa.solfagaming.model.response;

/**
 * Created by Ratri on 8/19/2017.
 */

public class ProfilResponse {

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    User user = new User();


    public class User {
       String id;
        String name;
        String email;
        String role;
        String state;
        String created_at;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        String updated_at;
    }
}
