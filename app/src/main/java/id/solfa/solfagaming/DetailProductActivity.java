package id.solfa.solfagaming;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.solfa.solfagaming.model.form.CartForm;
import id.solfa.solfagaming.model.response.CartResponse;
import id.solfa.solfagaming.model.response.ProductsResponse;
import id.solfa.solfagaming.adapter.ImageSliderAdapter;
import id.solfa.solfagaming.model.response.UpdateCartResponse;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import id.solfa.solfagaming.retrofit.URL;
import id.solfa.solfagaming.tools.FormatTools;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/10/2017.
 */

public class DetailProductActivity extends BaseActivity {
    ProductsResponse.Products products;
    ImageButton btnAddCart;
    Button btnBeli;
    TextView price, deskripsi;
    ViewPager vp_slider;
    LinearLayout ll_dots;
    FormatTools formatTools;
    ImageSliderAdapter imageAdapter;
    //    ArrayList<String> slider_image_list = new ArrayList<>();
    ArrayList<String> slider_image_list = new ArrayList<>();
    ArrayList<String> slider_url_list = new ArrayList<>();
    private TextView[] dots;
    int page_position = 0;
    private static int currentPage = 0;
    MenuItem itemCart;
    public LayerDrawable icon;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        Intent i= getIntent();
        if (i != null && i.getExtras() != null) {
            products = (ProductsResponse.Products) i.getSerializableExtra("model");
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        String title = products.getName();
        if (title.length()>25){
            title=title.substring(0,25) + "...";
        }
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnAddCart = (ImageButton) findViewById(R.id.add_cart) ;
        btnBeli = (Button) findViewById(R.id.btn_beli);
        price = (TextView) findViewById(R.id.price);
        deskripsi = (TextView) findViewById(R.id.deskripsi);
        price.setText(getRupiahFormat(products.getPrice()));
        deskripsi.setText(products.getDescription());

        vp_slider = (ViewPager) findViewById(R.id.vp_slider);
        ll_dots = (LinearLayout) findViewById(R.id.ll_dots);
        getBanner();
        addBottomDots(0);

        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (page_position == slider_image_list.size()) {
                    page_position = 0;
                } else {
                    page_position = page_position + 1;
                }
                vp_slider.setCurrentItem(page_position, true);
            }
        };

        btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLogin()){
                    CartForm cartForm = new CartForm();
                    cartForm.setProduct_id(products.getProduct_id());
                    cartForm.setAmount("1");
                    updateCart(cartForm);
                } else {
                    dialogLogin();
                }
            }
        });

        btnBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLogin()){
                    Intent i = new Intent(DetailProductActivity.this, CartActivity.class);
                    startActivity(i);
                } else {
                    dialogLogin();
                }
            }
        });
    }

    private void dialogLogin() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("LOGIN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent i = new Intent(DetailProductActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setMessage("Kamu harus login sebelum menambah keranjang belanja.");
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void getBanner() {
        slider_image_list.clear();
        slider_url_list.clear();
        for (int i =0; i<products.getImages().size(); i++){
            slider_image_list.add(URL.BASE_IMG+products.getImages().get(i).getPhoto());
        }
        initBanner();
    }

    private void initBanner() {
        imageAdapter = new ImageSliderAdapter(this, slider_image_list, slider_url_list);
        vp_slider.setAdapter(imageAdapter);

        vp_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void addBottomDots(int position) {
        dots = new TextView[slider_image_list.size()];

        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#000000"));
            ll_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
    }

    public  String getRupiahFormat (String i){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###,###");
        String formattedString = formatter.format(Double.parseDouble(i));
        formattedString = formattedString.replaceAll(",",".");
        return "IDR "+formattedString;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        itemCart = menu.findItem(R.id.menu_cart);
        icon = (LayerDrawable) itemCart.getIcon();

        //set cart
        if(isLogin()){
            setBadgeCount(this, icon, "0");
            getCartCount();

        }else{

        }
        return true;
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {
        Log.d("set cart", count);

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        Log.d("set cart", count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        noinspection SimplifiableIfStatement
        if (id == R.id.menu_cart) {
            Intent i = new Intent(DetailProductActivity.this, CartActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getCartCount() {
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<CartResponse> movieModelCall = allService.getCart(getToken());
        movieModelCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getMessage().matches("Successfully get user's cart.")){
                        List<CartResponse.Products> mItems =  new ArrayList<>();
                        System.out.println("response.body().getCarts().getProducts().toString()" + new Gson().toJson(response.body().getCarts().getProducts()));
                        mItems.addAll(response.body().getCarts().getProducts());
                        int total = 0;
                        for (int i = 0; i<response.body().getCarts().getProducts().size(); i++){
                            final CartResponse.Products results = mItems.get(i);
                            total = total + Integer.parseInt(results.getPivot().getAmount());
                        }
                        Log.d("cart 1", ""+total);
                        setCart(icon, String.valueOf(total));
                    } else {
                        cartCount = 0;
                        Log.d("cart 2", ""+cartCount);
                    }

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

            }
        });
    }

    private void setCart(LayerDrawable icon, String s) {
        setBadgeCount(this, icon, String.valueOf(s));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartCount();
    }

    private void updateCart(CartForm cartform) {
        AllService allService = ServiceGenerator.connectUpdateCart(AllService.class);
        Call<UpdateCartResponse> movieModelCall = allService.postAddCart(cartform, getToken());
        movieModelCall.enqueue(new Callback<UpdateCartResponse>() {
            @Override
            public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {

                System.out.println(response.message().toString());
                System.out.println(new Gson().toJson(response.body()));
                System.out.println(new Gson().toJson(response.message()));
                System.out.println(new Gson().toJson(response.errorBody()));

                if (response.isSuccessful()){
                    getCartCount();
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<UpdateCartResponse> call, Throwable t) {

            }
        });
    }
}
