package id.solfa.solfagaming;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Ratri on 11/25/2017.
 */

public class KonfirmasiPembayaranActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {
    Spinner spBankSolfa;
    EditText etPengirim, etBankPengirim, etNominal, etTanggal, etCatatan;
    ArrayList<String> arrayBank = new ArrayList<>();
    ArrayAdapter<CharSequence> adapterBank;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_pembayaran);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Konfirmasi Pembayaran");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spBankSolfa = (Spinner) findViewById(R.id.sp_bank) ;

        arrayBank.add("- Pilih Bank -");
        arrayBank.add("BNI an Ratri Retnowati");
        arrayBank.add("Mandiri an Farizal ");
        arrayBank.add("BCA an Ratri Retnowati");
        arrayBank.add("BTPN an Ratri Retnowati");
        arrayBank.add("Maybank an Ratri Retnowati");

        adapterBank = new ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayBank );
        adapterBank.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBankSolfa.setAdapter(adapterBank);

        spBankSolfa.setOnItemSelectedListener(this);

        etTanggal = (EditText) findViewById(R.id.et_tanggal);
        etTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        KonfirmasiPembayaranActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "Tanggal Tranfer");
            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        etTanggal.setText(year+"-"+(monthOfYear+1) +"-"+dayOfMonth);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
