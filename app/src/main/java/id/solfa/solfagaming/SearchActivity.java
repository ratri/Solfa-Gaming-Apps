package id.solfa.solfagaming;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.solfa.solfagaming.adapter.ProductListAdapter;
import id.solfa.solfagaming.adapter.SearchProductAdapter;
import id.solfa.solfagaming.model.response.CartResponse;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/15/2017.
 */

public class SearchActivity extends BaseActivity {
    RecyclerView rvNav, rvProducts;
    SearchProductAdapter productAdapter;
    int page=1;
    int loadmore=1;
    TextView tvKosong;
    MenuItem itemCart;
    LayerDrawable icon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_product);
        //Toolbar init
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Intent i = getIntent();
        String query="";
        if(i.getExtras()!=null){
            query=i.getExtras().getString("query");
            toolbar.setTitle(query);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //produk
        rvProducts = (RecyclerView)  findViewById(R.id.rv_products);
        productAdapter = new SearchProductAdapter(this, getToken());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        RecyclerView.LayoutManager friendsLayoutManager = new GridLayoutManager(this, 3);
        rvProducts.setLayoutManager(mLayoutManager);
        rvProducts.addItemDecoration(new SearchActivity.GridSpacingItemDecoration(1, dpToPx(1), true));
        rvProducts.setItemAnimator(new DefaultItemAnimator());
        rvProducts.setAdapter(productAdapter);
        productAdapter.initData(query);
        tvKosong=(TextView) findViewById(R.id.tv_kosong) ;

        //onloadmore
        rvProducts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount        = ((LinearLayoutManager) recyclerView.getLayoutManager()).getChildCount();
                int totalItemCount          = ((LinearLayoutManager) recyclerView.getLayoutManager()).getItemCount();
                int firstVisibleItemPosition= ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();;

                // Load more if we have reach the end to the recyclerView
                if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        });
    }

    public void setTvKosong(){
        tvKosong.setVisibility(View.VISIBLE);
        System.out.println("kosong ");
    }

    private void initProduct(String category) {

    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


    private void loadMoreItems() {
        if (loadmore>0){
            page = page + 1;
            loadmore = productAdapter.loadMore(page);
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        noinspection SimplifiableIfStatement
        if (id == R.id.menu_cart) {
            Intent i = new Intent(SearchActivity.this, CartActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        itemCart = menu.findItem(R.id.menu_cart);
        icon = (LayerDrawable) itemCart.getIcon();

        //set cart
        if(isLogin()){
            setBadgeCount(this, icon, "0");
            getCartCount();

        }else{

        }
        return true;
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {
        Log.d("set cart", count);

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        Log.d("set cart", count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    public void getCartCount() {
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<CartResponse> movieModelCall = allService.getCart(getToken());
        movieModelCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getMessage().matches("Successfully get user's cart.")){
                        List<CartResponse.Products> mItems =  new ArrayList<>();
                        System.out.println("response.body().getCarts().getProducts().toString()" + new Gson().toJson(response.body().getCarts().getProducts()));
                        mItems.addAll(response.body().getCarts().getProducts());
                        int total = 0;
                        for (int i = 0; i<response.body().getCarts().getProducts().size(); i++){
                            final CartResponse.Products results = mItems.get(i);
                            total = total + Integer.parseInt(results.getPivot().getAmount());
                        }
                        Log.d("cart 1", ""+total);
                        setCart(icon, String.valueOf(total));
                    } else {
                        cartCount = 0;
                        Log.d("cart 2", ""+cartCount);
                    }

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

            }
        });
    }

    private void setCart(LayerDrawable icon, String s) {
        setBadgeCount(this, icon, String.valueOf(s));
    }

}
