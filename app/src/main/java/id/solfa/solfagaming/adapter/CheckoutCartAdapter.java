package id.solfa.solfagaming.adapter;

/**
 * Created by Ratri on 10/12/2017.
 */

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.solfa.solfagaming.AddCheckoutActivity;
import id.solfa.solfagaming.holder.AddCheckoutHolder;
import id.solfa.solfagaming.model.response.CartResponse;
import id.solfa.solfagaming.model.response.UpdateCartResponse;
import id.solfa.solfagaming.model.form.CartForm;
import id.solfa.solfagaming.holder.CartHolder;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import id.solfa.solfagaming.retrofit.URL;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/10/2017.
 */

public class CheckoutCartAdapter extends ListAdapter<CartResponse.Products, AddCheckoutHolder> {
    AddCheckoutActivity activity;
    Context context;
    int total = 0;
    String token;


    public CheckoutCartAdapter(AddCheckoutActivity activity, String token){
        context = activity.getBaseContext();
        this.activity = activity;
        this.token = token;
    }

    @Override
    public AddCheckoutHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddCheckoutHolder(parent);
    }

    @Override
    public void onBindViewHolder(final AddCheckoutHolder holder, final int position) {

        final CartResponse.Products results = get(position);
        final List<CartResponse.Images> mItems =  new ArrayList<>();
        mItems.addAll(results.getImages());
        holder.tvNama.setText( replaceCharAt(results.getName()));
        holder.tvHarga.setText(activity.getRupiahFormat(""+results.getPrice()));
        holder.tvJumlah.setText(results.getPivot().getAmount());
        Glide.with(context).load(URL.BASE_IMG+mItems.get(0).getPhoto()).override(100, 100).into(holder.cover);
        int subTotal = Integer.valueOf(results.getPrice())* Integer.valueOf(results.getPivot().getAmount());
        holder.tvSubtotal.setText(activity.getRupiahFormat(""+subTotal));
    }

    private void updateCart(CartForm cartform, int position) {
        AllService allService = ServiceGenerator.connectUpdateCart(AllService.class);
        Call<UpdateCartResponse> movieModelCall = allService.postUpdateCart( cartform, token);
        movieModelCall.enqueue(new Callback<UpdateCartResponse>() {
            @Override
            public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {

                System.out.println(response.message().toString());
                System.out.println(new Gson().toJson(response.body()));
                System.out.println(new Gson().toJson(response.message()));
                System.out.println(new Gson().toJson(response.errorBody()));

                if (response.isSuccessful()){

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<UpdateCartResponse> call, Throwable t) {

            }
        });
    }

    public void initData(final String token){
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<CartResponse> movieModelCall = allService.getCart(token);
        movieModelCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getMessage().matches("Successfully get user's cart.")){
                        clearAll();
                        add(response.body().getCarts().getProducts());
                        total= 0;
                        for (int i = 0; i<response.body().getCarts().getProducts().size(); i++){
                            final CartResponse.Products results = get(i);
                            total = total + (Integer.parseInt(results.getPivot().getAmount())*Integer.parseInt(results.getPivot().getPrice()));
                        }
                        Log.d("total", String.valueOf(total));
                        activity.setButtonText(""+total, response.body().getCarts().getId() );
                    } else {
                        activity.setCartKosong();
                        activity.setButtonText("0", "");
                        clearAll();
                    }

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

            }
        });
    }

    public void getTotal(final String token){
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<CartResponse> movieModelCall = allService.getCart(token);
        movieModelCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getMessage().matches("Successfully get user's cart.")){
                        for (int i = 0; i<response.body().getCarts().getProducts().size(); i++){
                            final CartResponse.Products results = get(i);
                            total = total + (Integer.parseInt(results.getPivot().getAmount())*Integer.parseInt(results.getPivot().getPrice()));
                        }
                        activity.setButtonText(""+total, response.body().getCarts().getId() );
                    } else {
                        activity.setCartKosong();
                    }

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

            }
        });
    }

    public static String replaceCharAt(String s) {
        if (s.length()>30){
            s=s.substring(0,30) + "...";
        }
        return  s;
    }

}