package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import com.google.gson.Gson;

import id.solfa.solfagaming.MainActivity;
import id.solfa.solfagaming.model.response.CategoryResponse;
import id.solfa.solfagaming.holder.AdressHolder;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/11/2017.
 */

public class NavigationAdapter extends ListAdapter<CategoryResponse.Category, AdressHolder> {
    MainActivity activity;
    Context context;

    public NavigationAdapter(MainActivity activity){
        context = activity.getBaseContext();
        this.activity = activity;
    }

    @Override
    public AdressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdressHolder(parent);
    }

    @Override
    public void onBindViewHolder(AdressHolder holder, final int position) {
        final CategoryResponse.Category results = get(position);
        holder.txtNama.setText( results.getName());
        System.out.println(results.getName());
    }

    public void initData(){
        clearAll();
        AllService service = ServiceGenerator.connect(AllService.class);
        Call<CategoryResponse> movieModelCall =service.getCategory();
        movieModelCall.enqueue(new Callback<CategoryResponse>() {

            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if (response.isSuccessful()){
                    add(response.body().getCategories());
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {

            }
        });
    }
}
