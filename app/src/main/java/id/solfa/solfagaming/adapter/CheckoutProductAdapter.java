package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import id.solfa.solfagaming.AddCheckoutActivity;
import id.solfa.solfagaming.holder.CartHolder;
import id.solfa.solfagaming.holder.CheckoutProductHolder;
import id.solfa.solfagaming.model.response.CheckoutResponse;
import id.solfa.solfagaming.retrofit.URL;


/**
 * Created by Ratri on 10/22/2017.
 */

public class CheckoutProductAdapter  extends ListAdapter<CheckoutResponse.Product, CheckoutProductHolder > {
    AddCheckoutActivity activity;
    Context context;

    public  CheckoutProductAdapter(Context context){
        this.context = context;
    }

    @Override
    public CheckoutProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CheckoutProductHolder (parent);
    }

    @Override
    public void onBindViewHolder(final CheckoutProductHolder  holder, final int position) {

        final CheckoutResponse.Product results = get(position);
        holder.txt_nama.setText( replaceCharAt(results.getName()));
        holder.txt_harga.setText(getRupiahFormat(""+results.getPrice()));
        holder.txt_jumlah.setText(""+results.getPivot().getAmount());
        Glide.with(context).load(URL.BASE_IMG+results.getImages().get(0).getPhoto()).override(100, 100).into(holder.cover);
    }

    public void initData(CheckoutResponse.Checkout checkout ){
       add(checkout.getCart().getProducts());
    }

    public static String replaceCharAt(String s) {
        return s.substring(0,25) + "...";
    }

    public  String getRupiahFormat (String i){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###,###");
        String formattedString = formatter.format(Double.parseDouble(i));
        formattedString = formattedString.replaceAll(",",".");
        return "IDR "+formattedString;
    }
}