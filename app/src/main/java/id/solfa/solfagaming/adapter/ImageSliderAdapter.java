package id.solfa.solfagaming.adapter;

/**
 * Created by Ratri on 10/1/2017.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import id.solfa.solfagaming.DetailProductActivity;
import id.solfa.solfagaming.R;
import id.solfa.solfagaming.retrofit.URL;

public class ImageSliderAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    DetailProductActivity activity;
    ArrayList<String> image_arraylist;
    ArrayList<String> url_arraylist;

    public ImageSliderAdapter(DetailProductActivity activity, ArrayList<String> image_arraylist, ArrayList<String> url_arraylist) {
        this.activity = activity;
        this.image_arraylist = image_arraylist;
        this.url_arraylist = url_arraylist;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.item_slider, container, false);
        ImageView im_slider = (ImageView) view.findViewById(R.id.im_slider);
        Picasso.with(activity.getApplicationContext())
                .load(image_arraylist.get(position))
//                .placeholder(R.mipmap.ic_launcher) // optional
                .error(R.mipmap.ic_launcher)         // optional
                .into(im_slider);
//        im_slider.setImageResource(image_arraylist.get(position));
//        Glide.with(activity.getApplicationContext()).load(image_arraylist.get(position)).into(im_slider);
        im_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return image_arraylist.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}