package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.google.gson.Gson;

import id.solfa.solfagaming.model.response.AdressResponse;
import id.solfa.solfagaming.PilihAlamatActivity;
import id.solfa.solfagaming.holder.AdressHolder;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 8/24/2017.
 */

public class AdressAdapter extends ListAdapter<AdressResponse.Addresses, AdressHolder> {
    PilihAlamatActivity activity;
    Context context;
    String token;

    public AdressAdapter(PilihAlamatActivity activity,  String token){
        context = activity.getBaseContext();
        this.activity = activity;
        this.token = token;
    }

    @Override
    public AdressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        System.out.println("on create holder");
        return new AdressHolder(parent);
    }

    @Override
    public void onBindViewHolder(AdressHolder holder, final int position) {
        System.out.println("on bind holder");
        final AdressResponse.Addresses results = get(position);
        holder.txtNama.setText( results.getName());
        holder.txtEmail.setText(results.getEmail());
        holder.txtNoHp.setText(results.getPhone());
        holder.txtAlamat.setText(results.getAddress());
        System.out.println(results.getName());

//        holder.txtNama.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                results.checkout(activity);
//            }
//        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                results.checkout(activity);
            }
        });
    }

    public void initData(){
        System.out.println("init aderess");
        clearAll();
        AllService service = ServiceGenerator.connect(AllService.class);
        Call<AdressResponse> movieModelCall =service.getAdress(token);
        movieModelCall.enqueue(new Callback<AdressResponse>() {

            @Override
            public void onResponse(Call<AdressResponse> call, Response<AdressResponse> response) {
                System.out.println("resp"+response.message().toString());
                System.out.println("resp"+new Gson().toJson(response.body()));
                System.out.println("resp"+new Gson().toJson(response.message()));
                System.out.println("resp"+ response.body().getAddresses().size());

                if (response.isSuccessful()){
                    add(response.body().getAddresses());
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<AdressResponse> call, Throwable t) {
                System.out.println("fail"+ t);
            }
        });
    }
    }
