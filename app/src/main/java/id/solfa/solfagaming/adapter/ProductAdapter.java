package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.solfa.solfagaming.CartActivity;
import id.solfa.solfagaming.DetailProductActivity;
import id.solfa.solfagaming.MainActivity;
import id.solfa.solfagaming.model.response.ProductsResponse;
import id.solfa.solfagaming.model.response.UpdateCartResponse;
import id.solfa.solfagaming.model.form.CartForm;
import id.solfa.solfagaming.model.form.ProductSearchForm;
import id.solfa.solfagaming.holder.ProductHolder;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import id.solfa.solfagaming.retrofit.URL;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductAdapter extends ListAdapter<ProductsResponse.Products, ProductHolder> {
    MainActivity activity;
    Context context;
    int size =1;
    int total;
    String token;

    public ProductAdapter(MainActivity activity, String token){
        context = activity.getBaseContext();
        this.activity = activity;
        this.token = token;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductHolder(parent);
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, final int position) {

        final ProductsResponse.Products results = get(position);

        final List<ProductsResponse.Images> mItems =  new ArrayList<>();
        holder.txt_nama.setText( replaceCharAt(results.getName()));
        holder.txt_harga.setText("Rp "+results.getPrice());

        Glide.with(context).load(URL.BASE_IMG+results.getImages().get(0).getPhoto()).override(300, 100).into(holder.cover);
        holder.cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    results.detailProduk(activity);
            }
        });
        holder.cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activity.isLogin()){
                    CartForm cartForm = new CartForm();
                    cartForm.setProduct_id(results.getProduct_id());
                    cartForm.setAmount("1");
                    updateCart(cartForm);
                } else {
                    activity.dialogLogin("Kamu harus login sebelum menambah keranjang belanja.");
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                results.detailProduk(activity);
            }
        });

    }

    public static String replaceCharAt(String s) {
        return s.substring(0,14) + "...";
    }

    public void initData(){
        ProductSearchForm form = new ProductSearchForm();
        form.setPage(1);
        form.setLimit(6);
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<ProductsResponse> movieModelCall = allService.searchProduct(form);
        movieModelCall.enqueue(new Callback<ProductsResponse>() {

            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                if (response.isSuccessful()){
                    clearAll();
                    add(response.body().getProducts());
                    activity.hideProgress();
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                activity.hideProgress();
                Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }


    public int loadMore(int page) {
        ProductSearchForm form = new ProductSearchForm();
        form.setPage(page);
        form.setLimit(6);
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<ProductsResponse> movieModelCall = allService.searchProduct(form);
        movieModelCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                if (response.isSuccessful()){
                    ProductsResponse productsResponse = response.body();
                    size = productsResponse.getProducts().size();
                    if (productsResponse.getProducts().size()>0){
                        add(productsResponse.getProducts());
                    }
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
            }
        });
        return  size;
    }

    private void updateCart(CartForm cartform) {
        AllService allService = ServiceGenerator.connectUpdateCart(AllService.class);
        Call<UpdateCartResponse> movieModelCall = allService.postAddCart(cartform, token);
        movieModelCall.enqueue(new Callback<UpdateCartResponse>() {
            @Override
            public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {

                if (response.isSuccessful()){
                    Toast.makeText(context, "Sukses masuk keranjang" , Toast.LENGTH_LONG).show();
                    activity.getCartCount();
                } else Log.e("ERROR", "not success : "+response.raw());

            }

            @Override
            public void onFailure(Call<UpdateCartResponse> call, Throwable t) {
                Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
