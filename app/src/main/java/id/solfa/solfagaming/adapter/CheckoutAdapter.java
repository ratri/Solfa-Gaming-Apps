package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import id.solfa.solfagaming.CartActivity;
import id.solfa.solfagaming.CheckoutActivity;
import id.solfa.solfagaming.holder.CheckoutHolder;
import id.solfa.solfagaming.model.response.CartResponse;
import id.solfa.solfagaming.model.response.CheckoutResponse;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import id.solfa.solfagaming.retrofit.URL;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/21/2017.
 */

public class CheckoutAdapter extends ListAdapter<CheckoutResponse.Checkout, CheckoutHolder> {
    CheckoutActivity activity;
    Context context;
    String token;


    public CheckoutAdapter(CheckoutActivity activity, String token){
        context = activity.getBaseContext();
        this.activity = activity;
        this.token = token;
    }

    @Override
    public CheckoutHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        System.out.println("on create holder");
        return new CheckoutHolder(parent);
    }

    @Override
    public void onBindViewHolder(final CheckoutHolder holder, final int position) {
        final CheckoutResponse.Checkout checkout = get(position);
        holder.tvInvoice.setText("#" + checkout.getInvoice_no());
        holder.tvStatus.setText(""+checkout.getCheckout_state().toUpperCase());
        holder.tvTanggal.setText(checkout.getCreated_at());
        holder.tvNominal.setText(activity.getRupiahFormat(""+checkout.getSubtotal()));

        holder.tvNama.setText(checkout.getCart().getProducts().get(0).getName());
        holder.tvJumlah.setText(checkout.getCart().getProducts().get(0).getPivot().getAmount() +"x");
        holder.tvHarga.setText(activity.getRupiahFormat(""+checkout.getCart().getProducts().get(0).getPivot().getPrice()));
        Glide.with(context).load(URL.BASE_IMG+checkout.getCart().getProducts().get(0).getImages().get(0).getPhoto()).override(100, 100).into(holder.imgProduk);

        int total = 0;
        for (int i = 0; i<checkout.getCart().getProducts().size(); i++){
            total = total + (checkout.getCart().getProducts().get(i).getPivot().getAmount());
        }

        holder.tvJumlahTotal.setText(total+" produk");

//        holder.tvInvoice.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                checkout.checkoutDetail(context);
//            }
//        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkout.checkoutDetail(context);
            }
        });
    }

    public void initData(){
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<CheckoutResponse> modelCall = allService.checkout(token);
        modelCall.enqueue(new Callback<CheckoutResponse>() {
            @Override
            public void onResponse(Call<CheckoutResponse> call, Response<CheckoutResponse> response) {
                System.out.println(new Gson().toJson(response.body()));
                if (response.isSuccessful()){
                        clearAll();
                        add(response.body().getCheckouts());
                } else Log.e("ERROR", "not success : "+response.raw());
            }
            @Override
            public void onFailure(Call<CheckoutResponse> call, Throwable t) {
                Log.e("ERROR", "not success : "+t.toString());
            }
        });
    }
}