package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import id.solfa.solfagaming.MainActivity;
import id.solfa.solfagaming.R;
import id.solfa.solfagaming.retrofit.URL;

public class SliderBannerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    ArrayList<String> image_arraylist;
    ArrayList<String> url_arraylist;
    MainActivity activity;

    public SliderBannerAdapter(MainActivity activity, ArrayList<String> image_arraylist, ArrayList<String> url_arraylist) {
        this.activity = activity;
        this.image_arraylist = image_arraylist;
        this.url_arraylist = url_arraylist;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.layout_slider, container, false);
        ImageView im_slider = (ImageView) view.findViewById(R.id.im_slider);
        Picasso.with(activity.getApplicationContext())
                .load(URL.BASE_IMG_BANNER+image_arraylist.get(position))
                .error(R.mipmap.ic_launcher)         // optional
                .into(im_slider);
        im_slider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "";
//                if (url_arraylist.get(position).startsWith("http://")
//                        || url_arraylist.get(position).startsWith("https://")){
//                    url = url_arraylist.get(position);
//                } else {
//                    url = "http://"+url_arraylist.get(position);
//                }
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                activity.startActivity(i);
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return image_arraylist.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}