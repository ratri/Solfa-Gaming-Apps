package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.solfa.solfagaming.CartActivity;
import id.solfa.solfagaming.model.response.CartResponse;
import id.solfa.solfagaming.model.response.UpdateCartResponse;
import id.solfa.solfagaming.model.form.CartForm;
import id.solfa.solfagaming.holder.CartHolder;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import id.solfa.solfagaming.retrofit.URL;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ratri on 10/10/2017.
 */

public class CartAdapter extends ListAdapter<CartResponse.Products, CartHolder> {
    CartActivity activity;
    Context context;
    int total = 0;
    String token;


    public CartAdapter(CartActivity activity, String token){
        context = activity.getBaseContext();
        this.activity = activity;
        this.token = token;
    }

    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        System.out.println("on create holder");
        return new CartHolder(parent);
    }

    @Override
    public void onBindViewHolder(final CartHolder holder, final int position) {

        final CartResponse.Products results = get(position);
        final List<CartResponse.Images> mItems =  new ArrayList<>();
        mItems.addAll(results.getImages());
        holder.txt_nama.setText( replaceCharAt(results.getName()));
        holder.txt_harga.setText(activity.getRupiahFormat(results.getPrice()));
        holder.txt_jumlah.setText(results.getPivot().getAmount());
        Glide.with(context).load(URL.BASE_IMG+mItems.get(0).getPhoto()).override(100, 100).into(holder.cover);

        holder.tvMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int jumlah = Integer.valueOf(results.getPivot().getAmount())-1;
                CartForm cartform = new CartForm();
                cartform.setAmount(""+jumlah);
                cartform.setProduct_id(results.getPivot().getProduct_id());
                Log.d("id", results.getPivot().getProduct_id());
                updateCart(cartform, position);//retrofit update jumlah
                CartResponse.Pivot pivot = results.getPivot();
                pivot.setAmount(""+jumlah);
                get(position).setPivot(pivot);//coba
                holder.txt_jumlah.setText(results.getPivot().getAmount());

            }
        });
        holder.tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int jumlah = Integer.valueOf(results.getPivot().getAmount())+1;
                CartForm cartform = new CartForm();
                cartform.setAmount(""+jumlah);
                cartform.setProduct_id(results.getPivot().getProduct_id());
                Log.d("id", results.getPivot().getProduct_id());
                updateCart(cartform, position);//retrofit update jumlah
                CartResponse.Pivot pivot = results.getPivot();
                pivot.setAmount(""+jumlah);
                get(position).setPivot(pivot);//coba
                holder.txt_jumlah.setText(results.getPivot().getAmount());
            }
        });
    }

    private void updateCart(CartForm cartform, int position) {
        AllService allService = ServiceGenerator.connectUpdateCart(AllService.class);
        Log.d("token", activity.getToken() );
        Call<UpdateCartResponse> movieModelCall = allService.postUpdateCart( cartform, token);
        movieModelCall.enqueue(new Callback<UpdateCartResponse>() {
            @Override
            public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {

                if (response.isSuccessful()){
                    initData(activity.getToken());
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<UpdateCartResponse> call, Throwable t) {

            }
        });
    }

    public void initData(final String token){
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<CartResponse> movieModelCall = allService.getCart(token);
        movieModelCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getMessage().matches("Successfully get user's cart.")){
                        clearAll();
                        add(response.body().getCarts().getProducts());
                        total= 0;
                        for (int i = 0; i<response.body().getCarts().getProducts().size(); i++){
                            final CartResponse.Products results = get(i);
                            total = total + (Integer.parseInt(results.getPivot().getAmount())*Integer.parseInt(results.getPivot().getPrice()));
                        }
                        Log.d("total", String.valueOf(total));
                        activity.setButtonText(""+total);
                    } else {
                        activity.setCartKosong();
                        activity.setButtonText("0");
                        clearAll();
                    }

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

            }
        });
    }

    public void getTotal(final String token){
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<CartResponse> movieModelCall = allService.getCart(token);
        movieModelCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getMessage().matches("Successfully get user's cart.")){
                        for (int i = 0; i<response.body().getCarts().getProducts().size(); i++){
                            final CartResponse.Products results = get(i);
                            total = total + (Integer.parseInt(results.getPivot().getAmount())*Integer.parseInt(results.getPivot().getPrice()));
                        }
                        activity.setButtonText(""+total);
                    } else {
                        activity.setCartKosong();
                    }

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

            }
        });
    }

    public static String replaceCharAt(String s) {
        if (s.length()>30){
            s=s.substring(0,30) + "...";
        }
        return  s;
    }

}