package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.solfa.solfagaming.MainActivity;
import id.solfa.solfagaming.ProductListActivity;
import id.solfa.solfagaming.model.response.ProductsResponse;
import id.solfa.solfagaming.model.response.UpdateCartResponse;
import id.solfa.solfagaming.model.form.CartForm;
import id.solfa.solfagaming.model.form.ProductSearchForm;
import id.solfa.solfagaming.holder.ProductHolder;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import id.solfa.solfagaming.retrofit.URL;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListAdapter extends ListAdapter<ProductsResponse.Products, ProductHolder> {
    ProductListActivity activity;
    Context context;
    int size =1;
    int total;
    String token;

    public ProductListAdapter(ProductListActivity activity, String token){
        context = activity.getBaseContext();
        this.activity = activity;
        this.token = token;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        System.out.println("on create holder");
        return new ProductHolder(parent);
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, final int position) {

        final ProductsResponse.Products results = get(position);

        final List<ProductsResponse.Images> mItems =  new ArrayList<>();
//        mItems.addAll(results.getImages());
        holder.txt_nama.setText( replaceCharAt(results.getName()));
        holder.txt_harga.setText("Rp "+results.getPrice());
        if(results.getImages()!=null){
            System.out.println("tidak null");
            System.out.println("img ini tidak null" + URL.BASE_IMG+results.getImages().get(0).getPhoto());
        } else {
            System.out.println("null");
            System.out.println("img ini null" + results.getName());
        }
        Glide.with(context).load(URL.BASE_IMG+results.getImages().get(0).getPhoto()).override(300, 100).into(holder.cover);
//        Picasso.with(activity.getApplicationContext())
//                .load(URL.BASE_IMG+results.getImages().get(0).getPhoto())
//                .placeholder(R.mipmap.ic_launcher) // optional
//                .error(R.mipmap.ic_launcher)         // optional
//                .into(holder.cover);
        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                results.detailProduk(context);

            }
        });
        holder.cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CartForm cartForm = new CartForm();
                cartForm.setProduct_id(results.getProduct_id());
                cartForm.setAmount("1");
                updateCart(cartForm);
            }
        });

    }

    public static String replaceCharAt(String s) {
        return s.substring(0,14) + "...";
    }

    public void initData(String category){
        ProductSearchForm form = new ProductSearchForm();
        form.setPage(1);
        form.setLimit(6);
        form.setCategory_id(category);
        System.out.println("limit "+ form.getLimit() +"page "+form.getPage());
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<ProductsResponse> movieModelCall = allService.searchProduct(form);
        movieModelCall.enqueue(new Callback<ProductsResponse>() {

            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                if (response.isSuccessful()){
                    System.out.println(response.body().getProducts().size());
                    clearAll();
                    add(response.body().getProducts());
                    if (response.body().getProducts().size()==0){
                        activity.setTvKosong();
                        System.out.println("kosong " +response.body().getProducts().size());
                    }
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {

            }
        });
    }


    public int loadMore(int page) {
        Log.d("page", ""+page);
        ProductSearchForm form = new ProductSearchForm();
        form.setPage(page);
        form.setLimit(6);
        form.setQ("");
        System.out.println("limit "+ form.getLimit() +"page "+form.getPage());
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<ProductsResponse> movieModelCall = allService.searchProduct(form);
        movieModelCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                if (response.isSuccessful()){
                    System.out.println(response.body().getProducts().size());
                    ProductsResponse productsResponse = response.body();
                    size = productsResponse.getProducts().size();
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
            }
        });
        return  size;
    }

    private void updateCart(CartForm cartform) {
        AllService allService = ServiceGenerator.connectUpdateCart(AllService.class);
        Call<UpdateCartResponse> movieModelCall = allService.postAddCart(cartform, token);
        movieModelCall.enqueue(new Callback<UpdateCartResponse>() {
            @Override
            public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {

                System.out.println(response.message().toString());
                System.out.println(new Gson().toJson(response.body()));
                System.out.println(new Gson().toJson(response.message()));
                System.out.println(new Gson().toJson(response.errorBody()));

                if (response.isSuccessful()){
                    activity.getCartCount();
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<UpdateCartResponse> call, Throwable t) {

            }
        });
    }
}
