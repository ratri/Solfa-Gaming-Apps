package id.solfa.solfagaming.adapter;

import android.content.Context;
import android.view.ViewGroup;
import java.util.List;
import id.solfa.solfagaming.HelpActivity;
import id.solfa.solfagaming.model.ContactModel;
import id.solfa.solfagaming.holder.ContactHolder;


/**
 * Created by Ratri on 10/10/2017.
 */

public class ContactAdapter extends ListAdapter<ContactModel, ContactHolder> {
    HelpActivity activity;
    Context context;

    public ContactAdapter(HelpActivity activity) {
        context = activity.getBaseContext();
        this.activity = activity;
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactHolder(parent);
    }

    @Override
    public void onBindViewHolder(ContactHolder holder, final int position) {
        final ContactModel results = get(position);
        holder.imgContact.setImageResource(results.getImg());
    }

    public  void  init (List<ContactModel> contactModels){
        add(contactModels);
    }
}