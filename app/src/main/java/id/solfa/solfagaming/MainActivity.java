package id.solfa.solfagaming;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import id.solfa.solfagaming.model.response.CartResponse;
import id.solfa.solfagaming.model.response.CategoryResponse;
import id.solfa.solfagaming.adapter.ExpandableListAdapter;
import id.solfa.solfagaming.adapter.NavigationAdapter;
import id.solfa.solfagaming.adapter.ProductAdapter;
import id.solfa.solfagaming.adapter.SliderBannerAdapter;
import id.solfa.solfagaming.retrofit.AllService;
import id.solfa.solfagaming.retrofit.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener, View.OnClickListener {

    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ViewPager vp_slider;
    private LinearLayout ll_dots;
    SliderBannerAdapter sliderBannerAdapter;
    //    ArrayList<String> slider_image_list = new ArrayList<>();
    ArrayList<String> slider_image_list = new ArrayList<>();
    ArrayList<String> slider_url_list = new ArrayList<>();
    private TextView[] dots;
    int page_position = 0;
    private MenuItem mSearchMenuItem;
    ImageView imgDrawer;

    RecyclerView rvNav, rvProducts;
    ProductAdapter productAdapter;
    int page=1;
    int loadmore=1;
    SearchView mSearchView;
    public LayerDrawable icon;
    SharedPreferences sharedPreferences;
    public static final String TOKEN = "token";
    int cartCount;
    NavigationAdapter navigationAdapter;
    ExpandableListView expListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> listDataHeader = new ArrayList<String>();
    HashMap<String, List<String>> listDataChild = new HashMap<String, List<String>>();
    HashMap<String, List<String>> listDataChildIdCategory = new HashMap<String, List<String>>();
    DrawerLayout drawer;
    ImageView imgProfil;
    Button btnLogin, btnRegister;
    LinearLayout lnLoginRegister;
    TextView tvName;
    MenuItem itemCart;
    TextView tvAkun, tvBantuan, tvBerita, tvPembelian, tvSubcribe, tvLogout;
    ProgressBar progressBar;
    LinearLayout lnAlamat, lnProfil, lnHistory, lnLogout;
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Home");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sharedPreferences = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
        vp_slider = (ViewPager) findViewById(R.id.vp_slider);
        ll_dots = (LinearLayout) findViewById(R.id.ll_dots);
        rvProducts = (RecyclerView)  findViewById(R.id.rv_products);
        imgProfil = (ImageView) findViewById(R.id.profile_image);
        btnLogin =(Button) findViewById(R.id.btn_login);
        btnRegister =(Button) findViewById(R.id.btn_register);
        lnLoginRegister =(LinearLayout)  findViewById(R.id.layoutLoginRegister);
        tvName = (TextView)  findViewById(R.id.tv_name);
        tvBantuan = (TextView) findViewById(R.id.bantuan);
        tvSubcribe = (TextView) findViewById(R.id.subcribe) ;
        tvPembelian = (TextView) findViewById(R.id.pembelian);
        tvLogout = ( TextView) findViewById(R.id.logout) ;
        tvAkun = (TextView) findViewById(R.id.akun) ;
        progressBar = (ProgressBar) findViewById(R.id.progress);;

        lnAlamat = (LinearLayout) findViewById(R.id.ln_alamat) ;
        lnHistory = (LinearLayout) findViewById(R.id.ln_history);
        lnLogout = (LinearLayout) findViewById(R.id.ln_logout);
        lnProfil = (LinearLayout) findViewById(R.id.ln_profile) ;

        tvBantuan.setOnClickListener(this);
        tvSubcribe.setOnClickListener(this);
        tvPembelian.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        tvAkun.setOnClickListener(this);
        System.out.println(getToken());

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "id.solfa.solfagaming",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        //cek ijin
        cekPermision();

        FirebaseCrash.report(new Exception("My first Android non-fatal error"));

        //cek login
        if(isLogin()){
            pref = getSharedPreferences(BaseActivity.LOGIN_OPERATION, Context.MODE_PRIVATE);
            tvName.setText(pref.getString(BaseActivity.NAME, ""));
            lnLoginRegister.setVisibility(View.GONE);
        } else {
            lnHistory.setVisibility(View.GONE);
            lnAlamat.setVisibility(View.GONE);
            lnLogout.setVisibility(View.GONE);
            lnProfil.setVisibility(View.GONE);
            lnLoginRegister.setVisibility(View.VISIBLE);
            tvName.setText("Kamu belum login.");
        }

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
                dialogLogout();
            }
        });

        //login
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        //register
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent i = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

        //profil
        imgProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent i = new Intent(MainActivity.this, UserActivity.class);
                startActivity(i);
            }
        });

        tvAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
                Intent i = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(i);
            }
        });


        //ambil banner
        getBanner();
        addBottomDots(0);

        //produk
        productAdapter = new ProductAdapter(this, getToken());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        RecyclerView.LayoutManager friendsLayoutManager = new GridLayoutManager(this, 3);
        rvProducts.setLayoutManager(mLayoutManager);
        rvProducts.addItemDecoration(new MainActivity.GridSpacingItemDecoration(1, dpToPx(1), true));
        rvProducts.setItemAnimator(new DefaultItemAnimator());
        rvProducts.setAdapter(productAdapter);
        productAdapter.initData();

        // Category
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        initCategory();


        //onloadmore
        rvProducts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount        = ((LinearLayoutManager) recyclerView.getLayoutManager()).getChildCount();
                int totalItemCount          = ((LinearLayoutManager) recyclerView.getLayoutManager()).getItemCount();
                int firstVisibleItemPosition= ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();;

                // Load more if we have reach the end to the recyclerView
                if ( (visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        });

        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (page_position == slider_image_list.size()) {
                    page_position = 0;
                } else {
                    page_position = page_position + 1;
                }
                vp_slider.setCurrentItem(page_position, true);
            }
        };

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 100, 5000);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
    }

    private void dialogLogout() {
        AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent i = new Intent(MainActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.setTitle("Sukses");
        alert.setMessage("Kamu telah logout.");
        alert.setCancelMessage(null);
        alert.show();
    }

    public void dialogLogin(String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("LOGIN", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setMessage(s);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void hideProgress (){
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        mSearchMenuItem = menu.findItem(R.id.menu_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(mSearchMenuItem);
        mSearchView.setSubmitButtonEnabled(true);
        mSearchView.setOnQueryTextListener(this);
        itemCart = menu.findItem(R.id.menu_cart);
        icon = (LayerDrawable) itemCart.getIcon();

        //set cart
        if(isLogin()){
            setBadgeCount(this, icon, "0");
            getCartCount();
        }else{
            tvAkun.setVisibility(View.GONE);
            tvPembelian.setVisibility(View.GONE);
            tvLogout.setVisibility(View.GONE);
        }
        return true;
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {
        Log.d("set cart", count);

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        noinspection SimplifiableIfStatement
        if (id == R.id.menu_cart) {
            if (isLogin()){
                Intent i = new Intent(MainActivity.this, CartActivity.class);
                startActivity(i);
                return true;
            } else {
                dialogLogin("Kamu harus login sebelum ke keranjang belanja.");
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_home) {
//            // Handle the camera action
////        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_subcribe) {
//            Intent i = new Intent(MainActivity.this, SubcribeActivity.class);
//            startActivity(i);
//
//        } else if (id == R.id.nav_manage) {
//            Intent i = new Intent(MainActivity.this, LoginActivity.class);
//            startActivity(i);
//        } else if (id == R.id.nav_contact) {
//            Intent i = new Intent(MainActivity.this, HelpActivity.class);
//            startActivity(i);
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getBanner() {
        slider_image_list.clear();
        slider_url_list.clear();
        slider_image_list.add("promo.png");
//        slider_image_list.add("mabar.png");
        slider_image_list.add("steam_wallet.jpg");
        initBanner();
    }

    private void addBottomDots(int position) {
        dots = new TextView[slider_image_list.size()];

        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.parseColor("#000000"));
            ll_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#FFFFFF"));
    }

    private void initBanner() {
        sliderBannerAdapter = new SliderBannerAdapter(this, slider_image_list, slider_url_list);
        vp_slider.setAdapter(sliderBannerAdapter);

        vp_slider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        Intent i ;
        switch (view.getId()){
            case R.id.bantuan:
                i = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(i);
                break;
            case R.id.subcribe:
                i = new Intent(MainActivity.this, SubcribeActivity.class);
                startActivity(i);
                break;
            case  R.id.pembelian:
                i =new Intent(MainActivity.this, CheckoutActivity.class);
                startActivity(i);
                break;
        }
    }


    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void loadMoreItems() {
        if (loadmore>0){
            page = page + 1;
            loadmore = productAdapter.loadMore(page);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d("submit", query);
        Intent i = new Intent(MainActivity.this, SearchActivity.class);
        i.putExtra("query", query);
        startActivity(i);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    public void getCartCount() {
        AllService allService = ServiceGenerator.connect(AllService.class);
        Call<CartResponse> movieModelCall = allService.getCart(getToken());
        movieModelCall.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getMessage().matches("Successfully get user's cart.")){
                        List<CartResponse.Products> mItems =  new ArrayList<>();
                        System.out.println("response.body().getCarts().getProducts().toString()" + new Gson().toJson(response.body().getCarts().getProducts()));
                        mItems.addAll(response.body().getCarts().getProducts());
                        int total = 0;
                        for (int i = 0; i<response.body().getCarts().getProducts().size(); i++){
                            final CartResponse.Products results = mItems.get(i);
                            total = total + Integer.parseInt(results.getPivot().getAmount());
                        }
                       setCart(icon, String.valueOf(total));
                    } else {
                        cartCount = 0;
                        setCart(icon, String.valueOf(0));
                    }

                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

            }
        });
    }

    private void setCart(LayerDrawable icon, String s) {
        setBadgeCount(this, icon, String.valueOf(s));
    }

    /*
     * Preparing the list data
     */

    public void initCategory(){
        AllService service = ServiceGenerator.connect(AllService.class);
        Call<CategoryResponse> movieModelCall =service.getCategory();
        movieModelCall.enqueue(new Callback<CategoryResponse>() {

            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                System.out.println("resp"+response.message().toString());
                System.out.println("resp"+new Gson().toJson(response.body()));
                System.out.println("resp"+new Gson().toJson(response.message()));

                if (response.isSuccessful()){
                    System.out.println("suces");
                    for (int i=0; i<response.body().getCategories().size(); i++){
                        listDataHeader.add(response.body().getCategories().get(i).getName());
                        List<String> store = new ArrayList<String>();
                        List<String> storeId = new ArrayList<String>();
                        store.clear();
                        storeId.clear();
                        CategoryResponse.Category category = response.body().getCategories().get(i);
                        for (int b=0; b<category.getChildren().size(); b++){
                            store.add(category.getChildren().get(b).getName());
                            storeId.add(category.getChildren().get(b).getCategoryId());
                        }
                        listDataChild.put(listDataHeader.get(i), store); // Header, Child data
                        listDataChildIdCategory.put(listDataHeader.get(i), storeId); // Header, Child data
                        category();
                    }
                } else Log.e("ERROR", "not success : "+response.raw());
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                System.out.println("fail"+ t);
            }
        });
    }

    private void category() {
        expandableListAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild, listDataChildIdCategory);
        expListView.setAdapter(expandableListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartCount();
    }

    private void cekPermision() {
        if ((ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED)) {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(this, "Access denied!", Toast.LENGTH_SHORT).show();
                    cekPermision();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}


